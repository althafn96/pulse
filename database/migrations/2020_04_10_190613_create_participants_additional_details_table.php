<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantsAdditionalDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('participant_additional_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('participant_id');
            $table->string('title');
            $table->string('field_code');
            $table->string('options');
            $table->string('value');
            $table->integer('sort_order')->default('1000');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('participant_additional_details');
    }
}
