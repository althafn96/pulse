<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantGroupAttributesTable extends Migration
{
    public function up()
    {
        Schema::create('participant_group_attributes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('participant_group_id');
            $table->string('title')->nullable();
            $table->string('field_code')->nullable();
            $table->string('options')->nullable();
            $table->integer('sort_order')->default('1000');
            $table->integer('status')->default('1');
            $table->integer('temp_status')->default('0');
            $table->string('temp_title')->nullable();
            $table->string('temp_field_code')->nullable();
            $table->string('temp_options')->nullable();
            $table->integer('is_edit')->default('0');
            $table->integer('temp_sort_order')->default('1000');
            $table->integer('is_sort_order_change')->default('0');
            $table->integer('is_updated')->default('0');
            $table->integer('temp_is_deleted')->default('0');
        });
    }

    public function down()
    {
        Schema::dropIfExists('participant_group_attributes');
    }
}
