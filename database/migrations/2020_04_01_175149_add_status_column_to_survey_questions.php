<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusColumnToSurveyQuestions extends Migration
{

    public function up()
    {
        Schema::table('survey_questions', function (Blueprint $table) {
            $table->integer('q_status')->default('1');
        });
    }


    public function down()
    {
        Schema::table('survey_questions', function (Blueprint $table) {
            $table->dropColumn('q_status');
        });
    }
}
