<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyCategoryAttributesTempTable extends Migration
{

    public function up()
    {
        Schema::create('survey_category_attributes_temp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('added_user_id');
            $table->string('title');
            $table->string('field_code');
            $table->string('options');
            $table->integer('sort_order')->default('0');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('survey_category_attributes_temp');
    }
}
