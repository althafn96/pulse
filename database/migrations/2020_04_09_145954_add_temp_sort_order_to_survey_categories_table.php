<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTempSortOrderToSurveyCategoriesTable extends Migration
{
    public function up()
    {
        Schema::table('survey_category_attributes', function (Blueprint $table) {
            $table->integer('temp_sort_order')->default('100');
            $table->integer('is_sort_order_change')->default('0');
            $table->integer('is_updated')->default('0');
        });
    }

    public function down()
    {
        Schema::table('survey_category_attributes', function (Blueprint $table) {
            $table->dropColumn('temp_sort_order');
            $table->dropColumn('is_sort_order_change');
            $table->dropColumn('is_updated');
        });
    }
}
