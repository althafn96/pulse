<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveysTable extends Migration
{

    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->integer('survey_category_id');
            $table->integer('status')->default('2');
            $table->integer('is_deleted')->default('0');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}
