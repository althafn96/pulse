<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddSurveyCategoryTemp extends Migration
{
    public function up()
    {
        Schema::create('survey_category_temp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->integer('added_user_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('survey_category_temp');
    }
}
