<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTempDeleteColumnToSurveyCategoryAttributesTable extends Migration
{
    public function up()
    {
        Schema::table('survey_category_attributes', function (Blueprint $table) {
            $table->integer('temp_is_deleted')->default('0');
        });
    }

    public function down()
    {
        Schema::table('survey_category_attributes', function (Blueprint $table) {
            $table->dropColumn('temp_is_deleted');
        });
    }
}
