<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlugColumnToSurveyCategories extends Migration
{

    public function up()
    {
        Schema::table('survey_categories', function (Blueprint $table) {
            $table->string('slug')->nullable();
        });
    }


    public function down()
    {
        Schema::table('survey_categories', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
