<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantGroupAttributesTempTable extends Migration
{
    public function up()
    {
        Schema::create('participant_group_attributes_temp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('added_user_id');
            $table->string('title')->nullable();
            $table->string('field_code')->nullable();
            $table->string('options')->nullable();
            $table->integer('sort_order')->default('1000');
        });
    }

    public function down()
    {
        Schema::dropIfExists('participant_group_attributes_temp');
    }
}
