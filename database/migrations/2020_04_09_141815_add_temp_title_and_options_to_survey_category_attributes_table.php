<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTempTitleAndOptionsToSurveyCategoryAttributesTable extends Migration
{
    public function up()
    {
        Schema::table('survey_category_attributes', function (Blueprint $table) {
            $table->string('temp_title')->nullable();
            $table->string('temp_options')->nullable();
            $table->string('temp_field_code')->nullable();
            $table->integer('is_edit')->default('0');
        });
    }

    public function down()
    {
        Schema::table('survey_category_attributes', function (Blueprint $table) {
            $table->dropColumn('temp_title');
            $table->dropColumn('temp_options');
            $table->dropColumn('temp_field_code');
            $table->dropColumn('is_edit');
        });
    }
}
