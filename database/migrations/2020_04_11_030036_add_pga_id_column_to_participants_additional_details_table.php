<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPgaIdColumnToParticipantsAdditionalDetailsTable extends Migration
{
    public function up()
    {
        Schema::table('participant_additional_details', function (Blueprint $table) {
            $table->bigInteger('pga_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('participant_additional_details', function (Blueprint $table) {
            $table->dropColumn('pga_id');
        });
    }
}
