<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyQuestionsTable extends Migration
{

    public function up()
    {
        Schema::create('survey_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('survey_id');
            $table->string('question');
            $table->string('field_code');
            $table->integer('is_required')->default('0');
            $table->integer('comment_box')->default('0');
            $table->integer('points')->nullable();
            $table->integer('status')->default('0');
            $table->integer('sort_order')->default('1000');
        });
    }

    public function down()
    {
        Schema::dropIfExists('survey_questions');
    }
}
