<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTempFieldsToSurveyQuestionsTable extends Migration
{
    public function up()
    {
        Schema::table('survey_questions', function (Blueprint $table) {
            $table->integer('temp_status')->default('1');
            $table->string('temp_question')->nullable();
            $table->string('temp_field_code')->nullable();
            $table->string('temp_is_required')->nullable();
            $table->string('temp_comment_box')->nullable();
            $table->string('temp_points')->nullable();
            $table->string('temp_sort_order')->default('1000');
            $table->integer('temp_q_status')->default('1');
            $table->integer('temp_is_updated')->default('0');
            $table->integer('is_edit')->default('0');
            $table->integer('temp_is_sort_order_change')->default('0');
        });
    }


    public function down()
    {
        Schema::table('survey_questions', function (Blueprint $table) {
            $table->dropColumn('temp_status');
            $table->dropColumn('temp_question');
            $table->dropColumn('temp_field_code');
            $table->dropColumn('temp_is_required');
            $table->dropColumn('temp_comment_box');
            $table->dropColumn('temp_points');
            $table->dropColumn('temp_sort_order');
            $table->dropColumn('temp_q_status');
            $table->dropColumn('temp_is_updated');
            $table->dropColumn('is_edit');
            $table->dropColumn('temp_is_sort_order_change');
        });
    }
}
