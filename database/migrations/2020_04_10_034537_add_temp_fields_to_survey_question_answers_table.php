<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTempFieldsToSurveyQuestionAnswersTable extends Migration
{
    public function up()
    {
        Schema::table('survey_question_answers', function (Blueprint $table) {
            $table->string('temp_answer')->nullable();
            $table->string('temp_points')->nullable();
            $table->integer('temp_status')->default('1');
            $table->integer('temp_is_sort_order_change')->default('0');
            $table->integer('temp_sort_order')->default('1000');
            $table->integer('temp_is_deleted')->default('0');
        });
    }

    public function down()
    {
        Schema::table('survey_question_answers', function (Blueprint $table) {
            $table->dropColumn('temp_answer');
            $table->dropColumn('temp_points');
            $table->dropColumn('temp_status');
            $table->dropColumn('temp_is_sort_order_change');
            $table->dropColumn('temp_sort_order');
            $table->dropColumn('temp_is_deleted');
        });
    }
}
