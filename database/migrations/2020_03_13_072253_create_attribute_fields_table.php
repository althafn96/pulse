<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributeFieldsTable extends Migration
{

    public function up()
    {
        Schema::create('attribute_fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('icon');
            $table->string('code');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('attribute_fields');
    }
}
