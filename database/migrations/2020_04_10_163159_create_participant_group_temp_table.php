<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantGroupTempTable extends Migration
{
    public function up()
    {
        Schema::create('participant_group_temp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('added_user_id');
            $table->string('title')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('participant_group_temp');
    }
}
