<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyQuestionAnswersTable extends Migration
{

    public function up()
    {
        Schema::create('survey_question_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('question_id');
            $table->string('answer');
            $table->integer('points')->nullable();
            $table->integer('sort_order')->default('1000');
        });
    }

    public function down()
    {
        Schema::dropIfExists('survey_question_answers');
    }
}
