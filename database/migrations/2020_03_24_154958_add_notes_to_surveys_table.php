<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNotesToSurveysTable extends Migration
{

    public function up()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->longText('intro_note')->nullable();
            $table->longText('thankyou_note')->nullable();
        });
    }


    public function down()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->dropColumn('intro_note');
            $table->dropColumn('thankyou_note');
        });
    }
}
