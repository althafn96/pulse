<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBackgroundColumnToSurveysTable extends Migration
{
    public function up()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->string('background_image')->default('pulse_back.jpg');
        });
    }


    public function down()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->dropColumn('background_image');
        });
    }
}
