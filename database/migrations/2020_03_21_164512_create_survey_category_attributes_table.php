<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyCategoryAttributesTable extends Migration
{

    public function up()
    {
        Schema::create('survey_category_attributes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('survey_category_id');
            $table->string('title');
            $table->string('field_code');
            $table->string('options')->nullable();
            $table->integer('sort_order')->default('100');
            $table->integer('status')->default('1');
        });
    }


    public function down()
    {
        Schema::dropIfExists('survey_category_attributes');
    }
}
