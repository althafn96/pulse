<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyAdditionalDetailsTable extends Migration
{

    public function up()
    {
        Schema::create('survey_additional_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('survey_id');
            $table->string('title');
            $table->string('field_code');
            $table->integer('sort_order');
            $table->string('options')->nullable();
            $table->string('value')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('survey_additional_details');
    }
}
