<?php

use Illuminate\Database\Seeder;
use App\AttributeField;

class AttributesSeeder extends Seeder
{

    public function run()
    {
        $titles = array(
            'Single Textbox',
            'Multiple Choice',
            'Checkboxes',
            'Dropdown',
            'Date',
            'Time',
            'Rating'
        );

        $codes = array(
            '001',
            '002',
            '003',
            '004',
            '005',
            '006',
            '007',
        );

        $icons = array(
            'la la-square-o',
            'la la-list',
            'la la-check-square',
            'la la-sort',
            'la la-calendar-o',
            'la la-hourglass',
            'la la-star-o'
        );

        for ($i = 0; $i < sizeof($titles); $i++) {
            AttributeField::create([
                'title' => $titles[$i],
                'code' => $codes[$i],
                'icon' => $icons[$i]
            ]);
        }
    }
}
