<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnswerController extends Controller
{

    public function fetchAnswers(Request $request)
    {
        if ($request->has('answer')) {
            $answers = DB::table('answers')->where('answer', 'LIKE', '%' . $request->answer . '%')->get();
        } else {
            $answers = DB::table('answers')->get();
        }

        return $answers;
    }

    public function getQuestionAnswers($question_id)
    {
        $question = DB::table('survey_questions')->where('id', $question_id)->first();

        DB::table('survey_questions')->where('id', $question_id)->update([
            'temp_question' => $question->question,
            'temp_field_code' => $question->field_code,
            'temp_is_required' => $question->is_required,
            'temp_comment_box' => $question->comment_box,
            'temp_points' => $question->points
        ]);

        return DB::table('survey_question_answers')
            ->join('survey_questions', 'survey_questions.id', '=', 'survey_question_answers.question_id')
            ->where('question_id', $question_id)
            ->where('survey_question_answers.status', '1')
            ->where('survey_question_answers.temp_is_deleted', '0')
            ->select('survey_question_answers.*', 'survey_questions.id as question_id', 'survey_questions.question as question')
            ->orderBy('survey_question_answers.temp_sort_order', 'ASC')
            ->get();
    }

    public function checkIfAnswerExists($answer)
    {
        $if_answer_exists = DB::table('answers')->where('answer', $answer)->first();

        return $if_answer_exists;
    }

    public function storeAnswer($answer)
    {
        DB::table('answers')->insert([
            'answer' => $answer
        ]);
    }

    public function storeQuestionAnswer($question, $answer)
    {
        DB::table('survey_question_answers')->insert([
            'question_id' => $question,
            'answer' => $answer
        ]);
    }

    public function updateQuestionAnswers($question, $answer)
    {
        DB::table('survey_question_answers')->insert([
            'question_id' => $question,
            'answer' => $answer,
            'temp_answer' => $answer,
            'temp_status' => '1',
            'status' => '1'
        ]);
    }


    public function changeAnswerOrder(Request $request)
    {
        foreach ($request->toArray() as $id => $order) {
            DB::table('survey_question_answers')->where('id', $id)->update([
                'temp_sort_order' => $order,
                'temp_is_sort_order_change' => '1'
            ]);
        }
    }
}
