<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SurveyCategoryAttributes extends Controller
{
    public function addAttr(Request $request)
    {
        // dd($request->toArray());
        if ($request->title == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'title cant be empty'
            ]);
        }

        if ($request->attr == '002' || $request->attr == '003' || $request->attr == '004') {
            if (empty($request->options)) {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'options cant be empty'
                ]);
            }
        }

        if ($request->has('options')) {
            if (empty($request->options)) {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'options cant be empty'
                ]);
            }

            if (sizeof($request->options) < 2) {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'minimum of 2 options are required'
                ]);
            }
            $options = json_encode($request->options);
        } else {
            $options = '';
        }

        $insert_id = DB::table('survey_category_attributes')->insertGetId([
            'survey_category_id' => $request->survey_category_id,
            'title' => $request->title,
            'field_code' => $request->attr,
            'options' => $options,
            'sort_order' => '100',
            'temp_status' => '1'
        ]);

        $existing_survey_attribute = DB::table('survey_category_attributes')
            ->join('attribute_fields', 'attribute_fields.code', '=', 'survey_category_attributes.field_code')
            ->select('survey_category_attributes.*', 'attribute_fields.title as attr_title')
            ->where('survey_category_attributes.id', $insert_id)
            ->orderBy('sort_order', 'ASC')
            ->orderBy('survey_category_attributes.id', 'ASC')
            ->first();

        return response()->json([
            'type' => 'Success',
            'text' => 'attribute added successfully',
            'data' => $existing_survey_attribute
        ]);
    }

    public function editAttr(Request $request)
    {
        $sc_attr = DB::table('survey_category_attributes')->where('id', $request->id)->first();

        if ($sc_attr == null || $sc_attr == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'category attribute doesnt exist'
            ]);
        }

        $attribute_fields = DB::table('attribute_fields')->get();

        if ($sc_attr->is_edit == '0') {
            DB::table('survey_category_attributes')->where('id', $request->id)->update([
                'temp_title' => $sc_attr->title,
                'temp_field_code' => $sc_attr->field_code,
                'temp_options' => $sc_attr->options,
                'is_edit' => '1'
            ]);

            $sc_attr = DB::table('survey_category_attributes')->where('id', $request->id)->first();
        }

        return response()->json([
            'type' => 'Success',
            'text' => '',
            'data' => $sc_attr,
            'attribute_fields' => $attribute_fields
        ]);
    }

    public function updateAttr(Request $request)
    {
        if ($request->title == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'title cant be empty'
            ]);
        }

        if ($request->attr == "002" || $request->attr == "003" || $request->attr == "004") {
            if ($request->has('options')) {
                if ($request->options == []) {
                    return response()->json([
                        'type' => 'Error',
                        'text' => 'options cant be empty'
                    ]);
                }

                if (sizeof($request->options) < 2) {
                    return response()->json([
                        'type' => 'Error',
                        'text' => 'minimum of 2 options are required'
                    ]);
                }
                $options = json_encode($request->options);
            } else {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'options cant be empty'
                ]);
            }
        } else {
            $options = '';
        }

        DB::table('survey_category_attributes')->where('id', $request->id)->update([
            'temp_title' => $request->title,
            'temp_field_code' => $request->attr,
            'temp_options' => $options,
            'is_updated' => '1'
        ]);

        $survey_attribute = DB::table('survey_category_attributes')
            ->join('attribute_fields', 'attribute_fields.code', '=', 'survey_category_attributes.temp_field_code')
            ->select('survey_category_attributes.*', 'attribute_fields.title as attr_title')
            ->where('survey_category_attributes.id', $request->id)
            ->first();

        return response()->json([
            'type' => 'Success',
            'text' => 'attribute updated successfully',
            'data' => $survey_attribute
        ]);
    }

    public function removeAttr(Request $request)
    {
        // dd($request->toArray());
        DB::table('survey_category_attributes')->where('id', $request->id)->update([
            'temp_is_deleted' => '1'
        ]);

        return response()->json([
            'type' => 'Success',
            'text' => 'attribute removed successfully',
        ]);
    }

    public function changeAttrOrder(Request $request, $id)
    {
        foreach ($request->toArray() as $id => $order) {
            DB::table('survey_category_attributes')->where('id', $id)->update([
                'temp_sort_order' => $order,
                'is_sort_order_change' => '1'
            ]);
        }
    }
}
