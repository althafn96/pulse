<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $pageTitle = 'Dashboard';
        // $authenticated_user = Controller::get_authenticated_user();

        return view('index', compact('pageTitle'));
    }
}
