<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller
{

    public function fetchQuestions(Request $request)
    {
        if ($request->has('question')) {
            $questions = DB::table('questions')->where('question', 'LIKE', '%' . $request->question . '%')->get();
        } else {
            $questions = DB::table('questions')->get();
        }

        return $questions;
    }

    public function addQuestion(Request $request, $survey_id)
    {
        if ($request->question == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'question cant be empty'
            ]);
        }

        $if_question_exists = DB::table('questions')->where('question', $request->question)->first();

        if ($if_question_exists == '' || $if_question_exists == null) {
            DB::table('questions')->insert([
                'question' => $request->question
            ]);
        }

        $question_id = DB::table('survey_questions')->insertGetId([
            'survey_id' => $survey_id,
            'question' => $request->question,
            'field_code' => $request->attr,
            'is_required' => $request->is_required,
            'comment_box' => $request->comment_field,
        ]);

        if ($request->has('choices')) {
            $answersController = new AnswerController;
            foreach ($request->choices as $choice) {
                $if_answer_exists = $answersController->checkIfAnswerExists($choice);

                if ($if_answer_exists == '' || $if_answer_exists == null) {
                    $answersController->storeAnswer($choice);
                }

                $answersController->storeQuestionAnswer($question_id, $choice);
            }
        }

        $question = DB::table('survey_questions')->where('id', $question_id)->first();

        $choices = $request->has('choices') ? $request->choices : '';

        return response()->json([
            'type' => 'Success',
            'text' => 'question added succesfully',
            'question' => $question,
            'choices' => $choices
        ]);
    }

    public function changeQuestionOrder(Request $request)
    {
        foreach ($request->toArray() as $id => $order) {
            DB::table('survey_questions')->where('id', $id)->update([
                'temp_sort_order' => $order,
                'temp_is_sort_order_change' => '1'
            ]);
        }
    }

    public function editSurveyQuestion(Request $request)
    {
        $question = DB::table('survey_questions')->where('id', $request->id)->first();

        if ($question == null || $question == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'question doesnt exist'
            ]);
        }

        $answersController = new AnswerController;
        $answers = $answersController->getQuestionAnswers($question->id);
        $attribute_fields = DB::table('attribute_fields')->get();


        if ($question->is_edit == '0') {
            DB::table('survey_questions')->where('id', $request->id)->update([
                'temp_question' => $question->question,
                'temp_field_code' => $question->field_code,
                'temp_is_required' => $question->is_required,
                'temp_comment_box' => $question->comment_box,
                'temp_points' => $question->points,
                'is_edit' => '1'
            ]);

            $question = DB::table('survey_questions')->where('id', $request->id)->first();
        }

        return response()->json([
            'type' => 'Success',
            'text' => '',
            'question' => $question,
            'answers' => $answers,
            'attribute_fields' => $attribute_fields
        ]);
    }

    public function updateQuestion(Request $request)
    {
        if ($request->question == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'question cant be empty'
            ]);
        }

        $if_question_exists = DB::table('questions')->where('question', $request->question)->first();

        if ($if_question_exists == '' || $if_question_exists == null) {
            DB::table('questions')->insert([
                'question' => $request->question
            ]);
        }

        DB::table('survey_questions')->where('id', $request->id)->update([
            'temp_question' => $request->question,
            'temp_field_code' => $request->attr,
            'temp_is_required' => $request->is_required,
            'temp_comment_box' => $request->comment_field,
            'temp_is_updated' => '1',
        ]);



        if ($request->attr == "002" || $request->attr == "003" || $request->attr == "004") {

            if ($request->has('choices')) {
                DB::table('survey_question_answers')->where('question_id', $request->id)->update([
                    'temp_is_deleted' => '1'
                ]);

                $answersController = new AnswerController;
                foreach ($request->choices as $choice) {
                    $if_answer_exists =  $answersController->checkIfAnswerExists($choice);

                    if ($if_answer_exists == '' || $if_answer_exists == null) {
                        $answersController->storeAnswer($choice);
                    }

                    $answersController->updateQuestionAnswers($request->id, $choice);
                }
            } else {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'choices cant be empty'
                ]);
            }
        }

        $question = DB::table('survey_questions')->where('id', $request->id)->first();

        $choices = $request->has('choices') ? $request->choices : '';

        return response()->json([
            'type' => 'Success',
            'text' => 'question updated succesfully',
            'question' => $question,
            'choices' => $choices
        ]);
    }

    public function removeQuestion(Request $request)
    {
        $delete = DB::table('survey_questions')->where('id', $request->id)->update([
            'temp_q_status' => '0'
        ]);

        if ($delete == 1) {
            return response()->json([
                'type' => 'Success',
                'text' => 'question removed successfully',
                'data' => ''
            ]);
        } else {
            return response()->json([
                'type' => 'Error',
                'text' => 'unable to remove question. please try again later'
            ]);
        }
    }

    public function getQuestion(Request $request)
    {
        $question = DB::table('survey_questions')->where('id', $request->id)->first();

        $choices = DB::table('survey_question_answers')
            ->where('question_id', $request->id)
            ->where('status', '1')
            ->where('temp_is_deleted', '0')
            ->select('answer')
            ->orderBy('temp_sort_order', 'ASC')
            ->get();
        $choices_arr = array();

        foreach ($choices as $choice) {
            array_push($choices_arr, $choice->answer);
        }

        return response()->json([
            'type' => 'Success',
            'text' => '',
            'question' => $question,
            'choices' => $choices_arr
        ]);
    }
}
