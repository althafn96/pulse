<?php

namespace App\Http\Controllers;

use App\ParticipantGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use DataTables;

class ParticipantGroupController extends Controller
{
    public function index(Request $request)
    {
        $pageTitle = 'Participant Groups';

        if ($request->ajax()) {
            $participant_groups = DB::table('participant_groups')
                ->where('is_deleted', '0')
                ->get();

            return DataTables::of($participant_groups)
                ->addColumn('action', function ($group) {

                    return '';
                })
                ->rawColumns(['action'])
                ->make('true');
        }

        return view('participant-groups.index', compact('pageTitle'));
    }

    public function checkIfUnsavedExists()
    {
        $temp = DB::table('participant_group_temp')->where('added_user_id', auth()->id())->get();

        if ($temp->count() > 0) {
            return response()->json([
                'exists' => 'true',
                'text' => 'there is an unsaved participant group. Do you want to continue?'
            ]);
        } else {
            return response()->json([
                'exists' => 'false',
                'text' => ''
            ]);
        }
    }

    public function create(Request $request)
    {
        if (!$request->has('type')) {
            return redirect('participant-groups/create?type=new');
        }
        if ($request->type == 'new') {
            DB::table('participant_group_temp')->where('added_user_id', auth()->id())->delete();
            DB::table('participant_group_attributes_temp')->where('added_user_id', auth()->id())->delete();
        } else if ($request->type == 'continue') {
        } else {
            return redirect('participant-groups/create?type=new');
        }
        $pageTitle = 'Participant Groups';

        $temp_participant_group = DB::table('participant_group_temp')->where('added_user_id', auth()->id())->first();

        $attribute_fields = DB::table('attribute_fields')->get();

        $existing_participant_group_attributes = DB::table('participant_group_attributes_temp')
            ->join('attribute_fields', 'attribute_fields.code', '=', 'participant_group_attributes_temp.field_code')
            ->select('participant_group_attributes_temp.*', 'attribute_fields.title as attr_title')
            ->where('added_user_id', auth()->id())
            ->orderBy('sort_order', 'ASC')
            ->orderBy('participant_group_attributes_temp.id', 'ASC')
            ->get();

        return view('participant-groups.create', compact('pageTitle', 'temp_participant_group', 'attribute_fields', 'existing_participant_group_attributes'));
    }

    public function addTempPgTitle(Request $request)
    {
        $temp_pg = DB::table('participant_group_temp')->where('added_user_id', auth()->id())->first();

        if ($temp_pg == '' || $temp_pg == null) {
            $id = DB::table('participant_group_temp')
                ->insertGetId([
                    'title' => $request->pg_title,
                    'added_user_id' => auth()->id()
                ]);
        } else {
            $id = $temp_pg->id;
            DB::table('participant_group_temp')
                ->where('added_user_id', auth()->id())
                ->update([
                    'title' => $request->pg_title
                ]);
        }

        return response()->json([
            'id' => $id
        ]);
    }

    public function addTempAttr(Request $request)
    {
        if ($request->title == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'title cant be empty'
            ]);
        }

        if ($request->attr == '002' || $request->attr == '003' || $request->attr == '004') {
            if (!$request->has('options')) {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'options cant be empty'
                ]);
            }
        }
        if ($request->has('options')) {
            if ($request->options == []) {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'options cant be empty'
                ]);
            }

            if (sizeof($request->options) < 2) {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'minimum of 2 options are required'
                ]);
            }
            $options = json_encode($request->options);
        } else {
            $options = '';
        }

        $insert_id = DB::table('participant_group_attributes_temp')->insertGetId([
            'added_user_id' => auth()->id(),
            'title' => $request->title,
            'field_code' => $request->attr,
            'options' => $options,
            'sort_order' => '100'
        ]);

        $wxisting_participant_group_attr = DB::table('participant_group_attributes_temp')
            ->join('attribute_fields', 'attribute_fields.code', '=', 'participant_group_attributes_temp.field_code')
            ->select('participant_group_attributes_temp.*', 'attribute_fields.title as attr_title')
            ->where('participant_group_attributes_temp.id', $insert_id)
            ->orderBy('sort_order', 'ASC')
            ->orderBy('participant_group_attributes_temp.id', 'ASC')
            ->first();

        return response()->json([
            'type' => 'Success',
            'text' => 'attribute added successfully',
            'data' => $wxisting_participant_group_attr
        ]);
    }

    public function editTempAttr(Request $request)
    {
        $pg_attr = DB::table('participant_group_attributes_temp')->where('id', $request->id)->first();

        if ($pg_attr == null || $pg_attr == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'participant group doesnt exist'
            ]);
        }

        $pg_attr->temp_field_code = $pg_attr->field_code;
        $pg_attr->temp_title = $pg_attr->title;
        $pg_attr->temp_options = $pg_attr->options;

        $attribute_fields = DB::table('attribute_fields')->get();

        return response()->json([
            'type' => 'Success',
            'text' => '',
            'data' => $pg_attr,
            'attribute_fields' => $attribute_fields
        ]);
    }

    public function updateTempAttr(Request $request)
    {
        if ($request->title == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'title cant be empty'
            ]);
        }


        if ($request->attr == '002' || $request->attr == '003' || $request->attr == '004') {
            if (!$request->has('options')) {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'options cant be empty'
                ]);
            }
        }
        if ($request->has('options')) {
            if ($request->options == []) {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'options cant be empty'
                ]);
            }

            if (sizeof($request->options) < 2) {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'minimum of 2 options are required'
                ]);
            }
            $options = json_encode($request->options);
        } else {
            $options = '';
        }

        DB::table('participant_group_attributes_temp')->where('id', $request->id)->update([
            'added_user_id' => auth()->id(),
            'title' => $request->title,
            'field_code' => $request->attr,
            'options' => $options
        ]);

        $pg_attr = DB::table('participant_group_attributes_temp')
            ->join('attribute_fields', 'attribute_fields.code', '=', 'participant_group_attributes_temp.field_code')
            ->select('participant_group_attributes_temp.*', 'attribute_fields.title as attr_title')
            ->where('participant_group_attributes_temp.id', $request->id)
            ->first();

        $pg_attr->temp_field_code = $pg_attr->field_code;
        $pg_attr->temp_title = $pg_attr->title;
        $pg_attr->temp_options = $pg_attr->options;

        return response()->json([
            'type' => 'Success',
            'text' => 'attribute updated successfully',
            'data' => $pg_attr
        ]);
    }

    public function removeTempAttr(Request $request)
    {
        $delete = DB::table('participant_group_attributes_temp')->where('id', $request->id)->delete();

        if ($delete == 1) {

            return response()->json([
                'type' => 'Success',
                'text' => 'attribute removed successfully'
            ]);
        } else {
            return response()->json([
                'type' => 'Error',
                'text' => 'unable to remove attribute. please try again later'
            ]);
        }
    }

    public function store(Request $request)
    {
        $participant_group_temp = DB::table('participant_group_temp')->where('added_user_id', auth()->id())->first();

        $temp_attributes = DB::table('participant_group_attributes_temp')->where('added_user_id', auth()->id())->get();

        if ($participant_group_temp == null || $participant_group_temp == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'title cant be empty'
            ]);
        }

        DB::table('participant_group_temp')->where('added_user_id', auth()->id())->delete();
        DB::table('participant_group_attributes_temp')->where('added_user_id', auth()->id())->delete();

        $added_participant_group_id = DB::table('participant_groups')->insertGetId([
            'title' => $participant_group_temp->title
        ]);

        foreach ($temp_attributes as $attr) {
            DB::table('participant_group_attributes')->insert([
                'participant_group_id' => $added_participant_group_id,
                'title' => $attr->title,
                'field_code' => $attr->field_code,
                'options' => $attr->options,
                'sort_order' => $attr->sort_order
            ]);
        }

        return response()->json([
            'type' => 'Success',
            'text' => 'participant group added successfully'
        ]);
    }

    public function show(ParticipantGroup $participantGroup)
    {
        //
    }

    public function edit(ParticipantGroup $participantGroup)
    {
        $pageTitle = 'Participant Groups';

        $participant_group = DB::table('participant_groups')->where('id', $participantGroup->id)->first();

        if ($participant_group == null || $participant_group == null) {
            return view('error.404');
        }
        $attribute_fields = DB::table('attribute_fields')->get();

        DB::table('participant_group_attributes')->where('participant_group_id', $participantGroup->id)->where('temp_status', '1')->delete();

        DB::table('participant_group_attributes')->where('participant_group_id', $participantGroup->id)->update([
            'temp_title' => '',
            'temp_field_code' => '',
            'temp_options' => '',
            'is_edit' => '0',
            'is_updated' => '0',
            'is_sort_order_change' => '0',
            'temp_is_deleted' => '0'
        ]);

        $existing_pg_attributes = DB::table('participant_group_attributes')
            ->join('attribute_fields', 'attribute_fields.code', '=', 'participant_group_attributes.field_code')
            ->select('participant_group_attributes.*', 'attribute_fields.title as attr_title')
            ->where('participant_group_attributes.participant_group_id', $participantGroup->id)
            ->where('participant_group_attributes.status', '1')
            ->orderBy('sort_order', 'ASC')
            ->orderBy('participant_group_attributes.id', 'ASC')
            ->get();

        return view('participant-groups.edit', compact('pageTitle', 'participant_group', 'attribute_fields', 'existing_pg_attributes'));
    }

    public function update(Request $request, $id)
    {
        if ($request->pg_title == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'title cant be empty'
            ]);
        }
        DB::table('participant_groups')->where('id', $id)->update([
            'title' => $request->pg_title
        ]);

        $pg_attributes = DB::table('participant_group_attributes')->where('participant_group_id', $id)->get();
        foreach ($pg_attributes as $attr) {
            if ($attr->temp_status == '1') {
                DB::table('participant_group_attributes')->where('id', $attr->id)->update([
                    'temp_status' => '0',
                ]);
            }
            if ($attr->temp_is_deleted == '1') {
                DB::table('participant_group_attributes')->where('id', $attr->id)->update([
                    'status' => '0',
                    'temp_is_deleted' => '0',
                ]);
            }
            if ($attr->is_updated == '1') {
                DB::table('participant_group_attributes')->where('id', $attr->id)->update([
                    'title' => $attr->temp_title,
                    'field_code' => $attr->temp_field_code,
                    'options' => $attr->temp_options,
                    'temp_title' => '',
                    'temp_field_code' => '',
                    'temp_options' => '',
                    'is_updated' => '0',
                    'is_edit' => '0',
                    'temp_is_deleted' => '0',
                ]);
            }
            if ($attr->is_sort_order_change == '1') {
                DB::table('participant_group_attributes')->where('id', $attr->id)->update([
                    'sort_order' => $attr->temp_sort_order,
                    'is_sort_order_change' => '0',
                ]);
            }
        }

        return response()->json([
            'type' => 'Success',
            'text' => 'participant group updated successfully'
        ]);
    }

    public function destroy($id)
    {
        $delete = DB::table('participant_groups')->where('id', $id)->update([
            'is_deleted' => '1',
            'status' => '0'
        ]);

        if ($delete == 1) {
            return response()->json([
                'type' => 'Success',
                'text' => 'removed successfully'
            ]);
        } else {
            return response()->json([
                'type' => 'Error',
                'text' => 'process failed. please try again later'
            ]);
        }
    }

    public function changeStatus(Request $request)
    {
        $update = DB::table('participant_groups')->where('id', $request->id)->update([
            'status' => $request->changeTo
        ]);

        if ($update == 1) {
            return response()->json([
                'type' => 'Success',
                'text' => 'updated successfully'
            ]);
        } else {
            return response()->json([
                'type' => 'Error',
                'text' => 'update failed. please try again later'
            ]);
        }
    }

    public function getGroupAttributes(Request $request)
    {
        $pg_attributes = DB::table('participant_group_attributes')
            ->where('participant_group_id', $request->group_id)
            ->where('status', '1')
            ->where('temp_status', '0')
            ->orderBy('sort_order', 'ASC')
            ->get();

        return response()->json([
            'type' => 'Success',
            'data' => $pg_attributes
        ]);
    }
}
