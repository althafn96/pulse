<?php

namespace App\Http\Controllers;

use App\SurveyCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class SurveyCategoryController extends Controller
{
    public function index(Request $request)
    {
        $pageTitle = 'Survey Categories';

        if ($request->ajax()) {
            $survey_categories = DB::table('survey_categories')
                ->where('is_deleted', '0')
                ->get();

            // dd($survey_categories->toArray());

            return DataTables::of($survey_categories)
                ->addColumn('action', function ($survey_category) {

                    $button =  '';

                    return $button;
                })
                ->rawColumns(['action'])
                ->make('true');
        }

        return view('survey-categories.index', compact('pageTitle'));
    }

    public function getSurveyCategories(Request $request)
    {
        $survey_categories =  DB::table('survey_categories')->latest()->get()->toArray();

        return `
            {
                "meta": {
                    "page": 1,
                    "pages": 1,
                    "perpage": -1,
                    "total": 1,
                    "sort": "asc",
                    "field": "id"
                },
                "data": [
                    {
                        "id": 1,
                        "title": "fd",
                        "status": "1",
                    }
                ]
            }
        `;

        // dd($survey_categories);


    }

    public function create(Request $request)
    {
        if (!$request->has('type')) {
            return redirect('survey-categories/create?type=new');
        }
        if ($request->type == 'new') {
            DB::table('survey_category_temp')->where('added_user_id', auth()->id())->delete();
            DB::table('survey_category_attributes_temp')->where('added_user_id', auth()->id())->delete();
        } else if ($request->type == 'continue') {
        } else {
            return redirect('survey-categories/create?type=new');
        }

        $pageTitle = 'Survey Categories';

        $temp_survey_category = DB::table('survey_category_temp')->where('added_user_id', auth()->id())->first();

        $attribute_fields = DB::table('attribute_fields')->get();

        $existing_survey_attributes = DB::table('survey_category_attributes_temp')
            ->join('attribute_fields', 'attribute_fields.code', '=', 'survey_category_attributes_temp.field_code')
            ->select('survey_category_attributes_temp.*', 'attribute_fields.title as attr_title')
            ->where('added_user_id', auth()->id())
            ->orderBy('sort_order', 'ASC')
            ->orderBy('survey_category_attributes_temp.id', 'ASC')
            ->get();

        return view('survey-categories.create', compact('pageTitle', 'temp_survey_category', 'attribute_fields', 'existing_survey_attributes'));
    }

    public function loadSurveyCategoriesToSelect(Request $request)
    {
        if ($request->has('search')) {
            $survey_categories = DB::table('survey_categories')
                ->where('status', '1')
                ->where('title', 'like', '%' . $request->search . '%')
                ->get();
        } else {
            $survey_categories = DB::table('survey_categories')
                ->where('status', '1')
                ->get();
        }

        return $survey_categories;
    }

    public function store(Request $request)
    {
        $temp_survey_category = DB::table('survey_category_temp')->where('added_user_id', auth()->id())->first();

        $temp_attributes = DB::table('survey_category_attributes_temp')->where('added_user_id', auth()->id())->get();

        if ($temp_survey_category == null || $temp_survey_category == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'survey category title cant be empty'
            ]);
        }

        DB::table('survey_category_temp')->where('added_user_id', auth()->id())->delete();
        DB::table('survey_category_attributes_temp')->where('added_user_id', auth()->id())->delete();

        $added_survey_category_id = DB::table('survey_categories')->insertGetId([
            'title' => $temp_survey_category->title
        ]);

        foreach ($temp_attributes as $attr) {
            DB::table('survey_category_attributes')->insert([
                'survey_category_id' => $added_survey_category_id,
                'title' => $attr->title,
                'field_code' => $attr->field_code,
                'options' => $attr->options,
                'sort_order' => $attr->sort_order
            ]);
        }

        return response()->json([
            'type' => 'Success',
            'text' => 'survey category added successfully'
        ]);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $pageTitle = 'Survey Categories';

        $survey_category = DB::table('survey_categories')->where('id', $id)->first();

        if ($survey_category == null || $survey_category == null) {
            return view('error.404');
        }
        $attribute_fields = DB::table('attribute_fields')->get();

        DB::table('survey_category_attributes')->where('survey_category_id', $id)->where('temp_status', '1')->delete();

        DB::table('survey_category_attributes')->where('survey_category_id', $id)->update([
            'temp_title' => '',
            'temp_field_code' => '',
            'temp_options' => '',
            'is_edit' => '0',
            'is_updated' => '0',
            'is_sort_order_change' => '0',
            'temp_is_deleted' => '0'
        ]);

        $existing_survey_attributes = DB::table('survey_category_attributes')
            ->join('attribute_fields', 'attribute_fields.code', '=', 'survey_category_attributes.field_code')
            ->select('survey_category_attributes.*', 'attribute_fields.title as attr_title')
            ->where('survey_category_attributes.survey_category_id', $id)
            ->where('survey_category_attributes.status', '1')
            ->orderBy('sort_order', 'ASC')
            ->orderBy('survey_category_attributes.id', 'ASC')
            ->get();

        return view('survey-categories.edit', compact('pageTitle', 'survey_category', 'attribute_fields', 'existing_survey_attributes'));
    }

    public function editScTitle($id, Request $request)
    {
        dd($id);
    }

    public function update(Request $request, $id)
    {
        DB::table('survey_categories')->where('id', $id)->update([
            'title' => $request->sc_title
        ]);

        $sc_attributes = DB::table('survey_category_attributes')->where('survey_category_id', $id)->get();
        foreach ($sc_attributes as $attr) {
            if ($attr->temp_status == '1') {
                DB::table('survey_category_attributes')->where('id', $attr->id)->update([
                    'temp_status' => '0',
                ]);
            }
            if ($attr->temp_is_deleted == '1') {
                DB::table('survey_category_attributes')->where('id', $attr->id)->update([
                    'status' => '0',
                    'temp_is_deleted' => '0',
                ]);
            }
            if ($attr->is_updated == '1') {
                DB::table('survey_category_attributes')->where('id', $attr->id)->update([
                    'title' => $attr->temp_title,
                    'field_code' => $attr->temp_field_code,
                    'options' => $attr->temp_options,
                    'temp_title' => '',
                    'temp_field_code' => '',
                    'temp_options' => '',
                    'is_updated' => '0',
                    'is_edit' => '0',
                    'temp_is_deleted' => '0',
                ]);
            }
            if ($attr->is_sort_order_change == '1') {
                DB::table('survey_category_attributes')->where('id', $attr->id)->update([
                    'sort_order' => $attr->temp_sort_order,
                    'is_sort_order_change' => '0',
                ]);
            }
        }

        return response()->json([
            'type' => 'Success',
            'text' => 'survey category updated successfully'
        ]);
    }

    public function destroy($id)
    {
        $update = DB::table('survey_categories')->where('id', $id)->update([
            'is_deleted' => '1',
            'status' => '0'
        ]);

        if ($update == 1) {
            return response()->json([
                'type' => 'Success',
                'text' => 'removed successfully'
            ]);
        } else {
            return response()->json([
                'type' => 'Error',
                'text' => 'process failed. please try again later'
            ]);
        }
    }

    public function addTempScTitle(Request $request)
    {
        $temp_sc = DB::table('survey_category_temp')->where('added_user_id', auth()->id())->first();

        if ($temp_sc == '' || $temp_sc == null) {
            $id = DB::table('survey_category_temp')
                ->insertGetId([
                    'title' => $request->sc_title,
                    'added_user_id' => auth()->id()
                ]);
        } else {
            $id = $temp_sc->id;
            DB::table('survey_category_temp')
                ->where('added_user_id', auth()->id())
                ->update([
                    'title' => $request->sc_title
                ]);
        }

        return response()->json([
            'id' => $id
        ]);
    }

    public function addTempAttr(Request $request)
    {
        if ($request->title == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'title cant be empty'
            ]);
        }

        if ($request->has('options')) {
            if ($request->options == []) {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'options cant be empty'
                ]);
            }

            if (sizeof($request->options) < 2) {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'minimum of 2 options are required'
                ]);
            }
            $options = json_encode($request->options);
        } else {
            $options = '';
        }

        $insert_id = DB::table('survey_category_attributes_temp')->insertGetId([
            'added_user_id' => auth()->id(),
            'title' => $request->title,
            'field_code' => $request->attr,
            'options' => $options,
            'sort_order' => '100'
        ]);

        $existing_survey_attributes = DB::table('survey_category_attributes_temp')
            ->join('attribute_fields', 'attribute_fields.code', '=', 'survey_category_attributes_temp.field_code')
            ->select('survey_category_attributes_temp.*', 'attribute_fields.title as attr_title')
            ->where('survey_category_attributes_temp.id', $insert_id)
            ->orderBy('sort_order', 'ASC')
            ->orderBy('survey_category_attributes_temp.id', 'ASC')
            ->first();

        return response()->json([
            'type' => 'Success',
            'text' => 'attribute added successfully',
            'data' => $existing_survey_attributes
        ]);
    }

    public function getTempAttributes(Request $request)
    {
        // $atrr_fields = DB::table('')
    }

    public function changeTempAttrOrder(Request $request)
    {
        foreach ($request->toArray() as $id => $order) {
            DB::table('survey_category_attributes_temp')->where('id', $id)->update(['sort_order' => $order]);
        }
    }

    public function removeTempAttr(Request $request)
    {
        $insert = DB::table('survey_category_attributes_temp')->where('id', $request->id)->delete();

        if ($insert == 1) {
            $existing_survey_attributes = DB::table('survey_category_attributes_temp')
                ->join('attribute_fields', 'attribute_fields.code', '=', 'survey_category_attributes_temp.field_code')
                ->select('survey_category_attributes_temp.*', 'attribute_fields.title as attr_title')
                ->where('added_user_id', auth()->id())
                ->orderBy('sort_order', 'ASC')
                ->orderBy('survey_category_attributes_temp.id', 'ASC')
                ->get();

            return response()->json([
                'type' => 'Success',
                'text' => 'attribute removed successfully',
                'data' => $existing_survey_attributes
            ]);
        } else {
            return response()->json([
                'type' => 'Error',
                'text' => 'unable to remove attribute. please try again later'
            ]);
        }
    }

    public function editTempAttr(Request $request)
    {
        $sc_attr = DB::table('survey_category_attributes_temp')->where('id', $request->id)->first();

        if ($sc_attr == null || $sc_attr == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'category attribute doesnt exist'
            ]);
        }

        $attribute_fields = DB::table('attribute_fields')->get();

        return response()->json([
            'type' => 'Success',
            'text' => '',
            'data' => $sc_attr,
            'attribute_fields' => $attribute_fields
        ]);
    }

    public function updateTempAttr(Request $request)
    {
        if ($request->title == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'title cant be empty'
            ]);
        }

        if ($request->has('options')) {
            if ($request->options == []) {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'options cant be empty'
                ]);
            }

            if (sizeof($request->options) < 2) {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'minimum of 2 options are required'
                ]);
            }
            $options = json_encode($request->options);
        } else {
            $options = '';
        }

        DB::table('survey_category_attributes_temp')->where('id', $request->id)->update([
            'added_user_id' => auth()->id(),
            'title' => $request->title,
            'field_code' => $request->attr,
            'options' => $options
        ]);

        $survey_attribute = DB::table('survey_category_attributes_temp')
            ->join('attribute_fields', 'attribute_fields.code', '=', 'survey_category_attributes_temp.field_code')
            ->select('survey_category_attributes_temp.*', 'attribute_fields.title as attr_title')
            ->where('survey_category_attributes_temp.id', $request->id)
            ->first();

        return response()->json([
            'type' => 'Success',
            'text' => 'attribute updated successfully',
            'data' => $survey_attribute
        ]);
    }

    public function changeStatus(Request $request)
    {
        $update = DB::table('survey_categories')->where('id', $request->id)->update([
            'status' => $request->changeTo
        ]);

        if ($update == 1) {
            return response()->json([
                'type' => 'Success',
                'text' => 'updated successfully'
            ]);
        } else {
            return response()->json([
                'type' => 'Error',
                'text' => 'update failed. please try again later'
            ]);
        }
    }

    public function checkIfUnsavedExists()
    {
        $temp = DB::table('survey_category_temp')->where('added_user_id', auth()->id())->get();

        if ($temp->count() > 0) {
            return response()->json([
                'exists' => 'true',
                'text' => 'there is an unsaved survey category. Do you want to continue?'
            ]);
        } else {
            return response()->json([
                'exists' => 'false',
                'text' => ''
            ]);
        }
    }
}
