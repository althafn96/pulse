<?php

namespace App\Http\Controllers;

use App\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use DataTables;

class ParticipantController extends Controller
{
    public function index(Request $request)
    {
        $pageTitle = 'Participants';

        if ($request->ajax()) {
            $participants = DB::table('participants')
                ->join('participant_groups', 'participant_groups.id', '=', 'participants.participant_group_id')
                ->select('participants.*', 'participant_groups.title as group')
                ->where('participants.is_deleted', '0')
                ->get();

            return DataTables::of($participants)
                ->addColumn('name', function ($participant) {

                    return $participant->fname . ' ' . $participant->lname;
                })
                ->addColumn('action', function ($participant) {

                    return '';
                })
                ->rawColumns(['action'])
                ->make('true');
        }

        return view('participants.index', compact('pageTitle'));
    }

    public function create(Request $request)
    {
        if (!$request->has('type')) {
            return redirect('participants/create?type=new');
        }
        if ($request->type != 'new') {
            return redirect('participants/create?type=new');
        }

        $pageTitle = 'Participants';
        $participant_groups = DB::table('participant_groups')
            ->where('is_deleted', '0')
            ->get();

        return view('participants.create', compact('pageTitle', 'participant_groups'));
    }

    public function store(Request $request)
    {
        // dd($request->toArray());

        $p_id = DB::table('participants')->insertGetId([
            'title' => $request->participant_title,
            'fname' => $request->participant_fname,
            'lname' => $request->participant_lname,
            'email' => $request->participant_email,
            'participant_group_id' => $request->participant_group,
        ]);

        $pg_attributes = DB::table('participant_group_attributes')
            ->where('participant_group_id', $request->participant_group)
            ->where('status', '1')
            ->where('temp_status', '0')
            ->orderBy('sort_order', 'ASC')
            ->get();

        foreach ($pg_attributes as $attr) {
            DB::table('participant_additional_details')->insert([
                'participant_id' => $p_id,
                'title' => $attr->title,
                'field_code' => $attr->field_code,
                'options' => $attr->options,
                'value' => '',
                'sort_order' => $attr->sort_order,
                'pga_id' => $attr->id,
            ]);
        }

        $attr_fields = array();
        foreach ($request->toArray() as $key => $value) {
            if (strpos($key, 'attr-') !== false) {
                $attr_fields[$key] = $value;
            }
        }

        foreach ($attr_fields as $key => $value) {
            $attr_id = '';
            $attr = explode('-', $key);
            $attr_id = $attr['1'];

            $pad = DB::table('participant_additional_details')->where('pga_id', $attr_id)->where('participant_id', $p_id)->first();
            $field_code = $pad->field_code;

            if ($field_code == '003') {
                $current_value = '["' . $value . '"]';
                $prev_value = $pad->value;

                $value = $prev_value . ',' . $current_value;
                DB::table('participant_additional_details')->where('pga_id', $attr_id)->where('participant_id', $p_id)->update([
                    'value' => $value
                ]);
            } else {

                DB::table('participant_additional_details')->where('pga_id', $attr_id)->where('participant_id', $p_id)->update([
                    'value' => $value
                ]);
            }
        }

        return response()->json([
            'type' => 'Success',
            'text' => 'participant added successfully'
        ]);
    }

    public function show(Participant $participant)
    {
        //
    }

    public function edit(Participant $participant)
    {
        $pageTitle = 'Participants';

        $participant = Participant::find($participant->id);

        $participant_additional_details = DB::table('participant_additional_details')
            ->where('participant_id', $participant->id)
            ->get();

        return view('participants.edit', compact('pageTitle', 'participant', 'participant_additional_details'));
    }

    public function update(Request $request, Participant $participant)
    {
        //
    }

    public function destroy(Participant $participant)
    {
        $delete = DB::table('participants')->where('id', $participant->id)->update([
            'is_deleted' => '1',
            'status' => '0'
        ]);

        if ($delete == 1) {
            return response()->json([
                'type' => 'Success',
                'text' => 'removed successfully'
            ]);
        } else {
            return response()->json([
                'type' => 'Error',
                'text' => 'process failed. please try again later'
            ]);
        }
    }


    public function changeStatus(Request $request)
    {
        $update = DB::table('participants')->where('id', $request->id)->update([
            'status' => $request->changeTo
        ]);

        if ($update == 1) {
            return response()->json([
                'type' => 'Success',
                'text' => 'updated successfully'
            ]);
        } else {
            return response()->json([
                'type' => 'Error',
                'text' => 'update failed. please try again later'
            ]);
        }
    }
}
