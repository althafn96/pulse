<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class SurveyController extends Controller
{
    public function index(Request $request)
    {
        $pageTitle = 'Surveys';

        if ($request->ajax()) {
            $surveys = DB::table('surveys')
                ->join('survey_categories', 'survey_categories.id', '=', 'surveys.survey_category_id')
                ->select('surveys.*', 'survey_categories.title as category_title')
                ->where('surveys.is_deleted', '0')
                ->orderBy('surveys.id', 'DESC')
                ->get();

            // dd($surveys->toArray());

            return DataTables::of($surveys)
                ->addColumn('top', function ($survey) {

                    return '
                        <div class="kt-widget__label">
                            <div class="kt-widget__media kt-widget__media--m">
                                <span class="kt-media kt-media--md kt-media--circle kt-hidden-">
                                    <img src="' . asset('assets/uploads/') . '/' . $survey->background_image . '" alt="image">
                                </span>
                            </div>
                            <div class="kt-widget__info kt-padding-0 kt-margin-l-15">
                                <a href="#" class="kt-widget__title">
                                    ' . $survey->title . '
                                </a>
                                <span class="kt-widget__desc">
                                    ' . $survey->category_title . '
                                </span>
                            </div>
                        </div>
                        <div class="kt-widget__toolbar">
                            <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                <i class="flaticon-more-1"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                                            <span class="kt-nav__link-text">View</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="' . url("surveys/create/") . '/' . $survey->id . '" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-settings"></i>
                                            <span class="kt-nav__link-text">Design Survey</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-settings"></i>
                                            <span class="kt-nav__link-text">Remove</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                                            <span class="kt-nav__link-text">Report</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    ';
                })
                ->addColumn('middle', function ($survey) {
                    $ddd = '0';
                    if ($ddd == '0') {
                        $is_published = '
                        <div class="kt-widget__section">
                            <div class="kt-widget__action">
                                <button style="margin-left: 8rem" type="button" class="btn btn-label-brand btn-bold btn-sm btn-upper mt-3">Publish Survey</button>
                            </div>
                        </div>
                        ';
                    } else {
                        $is_published = '
                            <span class="kt-widget__subtitel">Completed</span>
                            <div class="kt-widget__progress d-flex align-items-center flex-fill">
                                <div class="progress" style="height: 5px;width: 100%;">
                                    <div class="progress-bar kt-bg-success" role="progressbar" style="width: 78%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="kt-widget__stat">
                                    78%
                                </span>
                            </div>
                        ';
                    }

                    $content =  '
                                <span class="kt-widget__text kt-margin-t-0 kt-padding-t-5">
                                ' . $survey->description . '
                                </span>
                                <div class="kt-widget__stats kt-margin-t-20">
                                    <div class="kt-widget__item d-flex align-items-center kt-margin-r-30">
                                        <span class="kt-widget__date kt-padding-0 kt-margin-r-10">
                                            Start
                                        </span>
                                        <div class="kt-widget__label">
                                            <span class="btn btn-label-brand btn-sm btn-bold btn-upper">' . $survey->start_date . '</span>
                                        </div>
                                    </div>
                                    <div class="kt-widget__item d-flex align-items-center kt-padding-l-0">
                                        <span class="kt-widget__date kt-padding-0 kt-margin-r-10 ">
                                            Due
                                        </span>
                                        <div class="kt-widget__label">
                                            <span class="btn btn-label-danger btn-sm btn-bold btn-upper">' . $survey->end_date . '</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-widget__container">
                                ' . $is_published . '
                                </div>
                    ';

                    return $content;
                })
                ->addColumn('bottom', function ($survey) {
                    $survey_questions = DB::table('survey_questions')->where('survey_id', $survey->id)->where('q_status', '1')->where('temp_status', '0')->get();

                    $content =  '
                    <div class="kt-widget__wrapper">
                        <div class="kt-widget__section">
                            <div class="kt-widget__blog">
                                <i class="flaticon2-list-1"></i>
                                <a href="#" class="kt-widget__value kt-font-brand">' . $survey_questions->count() . ' Questions </a>
                            </div>
                            <div class="kt-widget__blog">
                                <i class="flaticon2-talk"></i>
                                <a href="#" class="kt-widget__value kt-font-brand">10 Groups</a>
                            </div>
                        </div>
                    </div>
                    ';

                    return $content;
                })
                ->rawColumns(['top', 'middle', 'bottom'])
                ->make('true');
        }

        return view('surveys.index', compact('pageTitle'));
    }

    public function getSurveys()
    {
        dd('d');
    }

    public function createSurvey(Request $request)
    {
        if ($request->title == '') {
            return response()->json([
                'type' => 'Error',
                'text' => 'survey title cant be empty'
            ]);
        }
        if ($request->survey_category == '0') {
            return response()->json([
                'type' => 'Error',
                'text' => 'survey category cant be empty'
            ]);
        }

        $survey_id = DB::table('surveys')->insertGetId([
            'title' => $request->title,
            'survey_category_id' => $request->survey_category
        ]);


        $category_attributes = DB::table('survey_category_attributes')
            ->where('survey_category_id', $request->survey_category)
            ->where('status', '1')
            ->get();

        foreach ($category_attributes as $attr) {
            DB::table('survey_additional_details')->insert([
                'survey_id' => $survey_id,
                'title' => $attr->title,
                'field_code' => $attr->field_code,
                'sort_order' => $attr->sort_order,
                'options' => $attr->options
            ]);
        }


        return response()->json([
            'type' => 'Info',
            'text' => 'Hang on while we set things up for you...',
            'survey_id' => $survey_id
        ]);
    }

    public function designSurvey($id)
    {
        $pageTitle = 'Surveys';

        $survey = DB::table('surveys')
            ->where('id', $id)
            ->first();

        if ($survey == null || $survey == '') {
            return abort('404');
        }

        $attribute_fields = DB::table('attribute_fields')->get();

        $category_attributes = DB::table('survey_additional_details')
            ->where('survey_id', $survey->id)
            ->orderBy('sort_order', 'ASC')
            ->get();

        DB::table('survey_questions')->where('survey_id', $id)->where('temp_status', '1')->delete();

        DB::table('survey_questions')->where('survey_id', $id)->update([
            'is_edit' => '0',
            'temp_is_updated' => '0',
            'temp_is_sort_order_change' => '0',
            'temp_q_status' => '1',
            'temp_question' => '',
            'temp_field_code' => '',
            'temp_is_required' => '',
            'temp_comment_box' => '',
        ]);
        $survey_questions = DB::table('survey_questions')->where('survey_id', $id)->where('q_status', '1')->orderBy('sort_order', 'ASC')->get();

        foreach ($survey_questions as $question) {

            $answers = DB::table('survey_question_answers')->where('question_id', $question->id)->where('status', '1')->get();

            foreach ($answers as $answer) {
                DB::table('survey_question_answers')->where('id', $answer->id)->update([
                    'temp_sort_order' => $answer->sort_order,
                    'temp_is_sort_order_change' => '0',
                ]);
            }
            $answers = DB::table('survey_question_answers')->where('question_id', $question->id)->where('status', '1')->orderBy('temp_sort_order', 'ASC')->get();


            // dd($answers);

            DB::table('survey_question_answers')->where('question_id', $question->id)->where('temp_is_deleted', '1')->update([
                'temp_is_deleted' => '0'
            ]);
            DB::table('survey_question_answers')->where('question_id', $question->id)->update([
                'temp_is_sort_order_change' => '0'
            ]);
            DB::table('survey_question_answers')->where('question_id', $question->id)->where('temp_status', '1')->delete();

            $question->answers = $answers;
        }

        return view('surveys.design', compact('pageTitle', 'survey', 'attribute_fields', 'category_attributes', 'survey_questions'));
    }

    public function update(Request $request, $survey_id)
    {
        // dd($request->toArray());
        $survey = DB::table('surveys')->where('id', $survey_id)->first();

        if ($request->hasFile('survey_background_image')) {
            $picture = $request->background_image;

            $picture_new_name = time() . $picture->getClientOriginalName();

            $picture->move('assets/uploads', $picture_new_name);
        } else {
            $picture_new_name = $survey->background_image;
        }

        if ($request->intro_note == '<p><br></p>') {
            return response()->json([
                'type' => 'Error',
                'text' => 'intro note cant be empty'
            ]);
        }
        if ($request->thankyou_note == '<p><br></p>') {
            return response()->json([
                'type' => 'Error',
                'text' => 'thank you note cant be empty'
            ]);
        }

        if ($request->survey_is_scheduled == '1') {
            if ($request->survey_start_date_time == '') {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'survey start date and time cant be empty'
                ]);
            }
            if ($request->survey_end_date_time == '') {
                return response()->json([
                    'type' => 'Error',
                    'text' => 'survey end date and time cant be empty'
                ]);
            }

            $start_arr = explode(' ', $request->survey_start_date_time);
            $end_arr = explode(' ', $request->survey_end_date_time);

            $start_date = $start_arr[0];
            $start_time = $start_arr[1];
            $end_date = $end_arr[0];
            $end_time = $end_arr[1];
        } else {
            $start_date = NULL;
            $start_time = NULL;
            $end_date = NULL;
            $end_time = NULL;
        }

        DB::table('surveys')->where('id', $survey_id)->update([
            'description' => $request->survey_description,
            'intro_note' => $request->survey_intro_note,
            'thankyou_note' => $request->survey_thankyou_note,
            'background_image' => $picture_new_name,
            'is_scheduled' => $request->survey_is_scheduled,
            'start_date' => $start_date,
            'start_time' => $start_time,
            'end_date' => $end_date,
            'end_time' => $end_time,
        ]);

        $attr_fields = array();
        foreach ($request->toArray() as $key => $value) {
            if (strpos($key, 'attr-') !== false) {
                $attr_fields[$key] = $value;
            }
        }

        DB::table('survey_additional_details')->where('survey_id', $survey_id)->update([
            'value' => ''
        ]);

        foreach ($attr_fields as $key => $value) {
            $attr_id = '';
            $attr = explode('-', $key);
            $attr_id = $attr['1'];

            $sad = DB::table('survey_additional_details')->where('id', $attr_id)->first();
            $field_code = $sad->field_code;

            if ($field_code == '003') {
                $current_value = '["' . $value . '"]';
                $prev_value = $sad->value;

                $value = $prev_value . ',' . $current_value;
                DB::table('survey_additional_details')->where('id', $attr_id)->update([
                    'value' => $value
                ]);
            } else {

                DB::table('survey_additional_details')->where('id', $attr_id)->update([
                    'value' => $value
                ]);
            }
        }

        $questions = DB::table('survey_questions')->where('survey_id', $survey_id)->get();

        foreach ($questions as $question) {
            if ($question->temp_status == '1') {
                DB::table('survey_questions')->where('id', $question->id)->update([
                    'temp_status' => '0',
                ]);
            }
            if ($question->temp_q_status == '0') {
                DB::table('survey_questions')->where('id', $question->id)->update([
                    'status' => '0',
                    'q_status' => '0',
                    'temp_q_status' => '0'
                ]);
            }
            if ($question->temp_is_updated == '1') {
                DB::table('survey_questions')->where('id', $question->id)->update([
                    'question' => $question->temp_question,
                    'field_code' => $question->temp_field_code,
                    'is_required' => $question->temp_is_required,
                    'comment_box' => $question->temp_comment_box,
                    'temp_question' => '',
                    'temp_field_code' => '',
                    'temp_is_required' => '',
                    'temp_comment_box' => '',
                    'temp_is_updated' => '0',
                    'is_edit' => '0',
                    'temp_q_status' => '1',
                ]);
            }
            if ($question->temp_is_sort_order_change == '1') {
                DB::table('survey_questions')->where('id', $question->id)->update([
                    'sort_order' => $question->temp_sort_order,
                    'temp_is_sort_order_change' => '0',
                ]);
            }

            DB::table('survey_question_answers')->where('question_id', $question->id)->where('temp_status', '1')->update([
                'status' => '1',
                'temp_status' => '0',
            ]);

            $answers = DB::table('survey_question_answers')->where('question_id', $question->id)->get();

            foreach ($answers as $answer) {
                if ($answer->temp_is_sort_order_change == '1') {
                    DB::table('survey_question_answers')->where('id', $answer->id)->update([
                        'sort_order' => $answer->temp_sort_order,
                        'temp_is_sort_order_change' => '0'
                    ]);
                }
            }

            DB::table('survey_question_answers')->where('question_id', $question->id)->where('temp_is_deleted', '1')->update([
                'status' => '0'
            ]);
        }

        return response()->json([
            'type' => 'Success',
            'text' => 'survey updated successfully'
        ]);
    }
}
