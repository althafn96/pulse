<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyCategory extends Model
{
    public function survey()
    {
        return $this->belongsTo('App\Survey');
    }
}
