<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    public function surveyCategory()
    {
        return $this->hasOne('App\SurveyCategory');
    }
}
