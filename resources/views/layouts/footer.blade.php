
    <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-footer__copyright">
                {{ date('Y') }}&nbsp;&copy;&nbsp;<a href="http://pulse.pragicts.com/" style="color: #f71735" target="_blank" class="kt-link">Pulse</a>&nbsp; v 1.0
            </div>
            <div class="kt-footer__menu">
                Engineered by &nbsp;<a href="https://pragicts.com/" style="color: #f71735; margin: 0" target="_blank" class="kt-footer__menu-link kt-link">PragICTS</a>
            </div>
        </div>
    </div>
    
</div>