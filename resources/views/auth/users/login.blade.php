<!DOCTYPE html>
<html lang="en">

	<head>
		<base href="../../../">
		<meta charset="utf-8" />
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<title>{{ config('app.name') }} | Sign In</title>
		<meta name="description" content="Login page example">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

		<link href="{{ asset('assets/css/pages/login/login-1.css') }}" rel="stylesheet" type="text/css" />

		<link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

		<link href="{{ asset('assets/css/skins/header/base/light.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/css/skins/header/menu/light.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/css/skins/brand/dark.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/css/skins/aside/dark.css') }}" rel="stylesheet" type="text/css" />

		<link rel="shortcut icon" href="{{ asset('assets/media/logos/pulse-icon.png') }}" />
	</head>

	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

		<div class="kt-grid kt-grid--ver kt-grid--root">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">

					<div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url({{ asset('assets/media/bg/login_bg.jpg') }});">
						<div class="kt-grid__item">
							<a href="#" onclick="return false;" class="kt-login__logo">
								<img style="width: 150px !important" src="{{ asset('assets/media/logos/pulse-logo.png') }}">
							</a>
						</div>
						<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
							<div class="kt-grid__item kt-grid__item--middle">
								<h3 class="kt-login__title">Welcome to Pulse!</h3>
								<h4 class="kt-login__subtitle">
                                    The ultimate feedback platform to keep a tab on your performance and for prompt corrective action
                                </h4>
							</div>
						</div>
						<div class="kt-grid__item">
							<div class="kt-login__info">
								<div class="kt-login__copyright">
									{{ date('Y') }}&nbsp;&copy;&nbsp;<a href="http://pulse.pragicts.com/" style="color: #f71735" target="_blank" class="kt-link">Pulse</a>. 
								</div>
								<div class="kt-login__menu">
									<span style="color: rgba(255, 255, 255, 0.4)" >Engineered by</span> &nbsp;<a href="https://pragicts.com/" style="color: #f71735; margin: 0" target="_blank" class="kt-link">PragICTS</a>
								</div>
							</div>
						</div>
					</div>

					<div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">

						<div class="kt-login__body">

							<div class="kt-login__form">
								<div class="kt-login__title">
									<h3>Sign In</h3>
								</div>

								<form class="kt-form" novalidate="novalidate" id="kt_login_form">
									<div class="form-group">
										<input class="form-control" type="text" placeholder="Username" name="email" id="email" autocomplete="off">
									</div>
									<div class="form-group">
										<input class="form-control" type="password" placeholder="Password" name="password" id="password" autocomplete="off">
									</div>

									<div class="kt-login__actions">
										<a href="#" class="kt-link kt-login__link-forgot">
											Forgot Password ?
										</a>
										<button dataUrl="{{ url('/') }}" id="kt_login_signin_submit" class="btn btn-primary btn-elevate kt-login__btn-primary">Sign In</button>
									</div>

								</form>

							</div>

						</div>

					</div>

				</div>
			</div>
		</div>

		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": [
							"#c5cbe3",
							"#a1a8c3",
							"#3d4465",
							"#3e4466"
						],
						"shape": [
							"#f0f3ff",
							"#d9dffa",
							"#afb4d4",
							"#646c9a"
						]
					}
				}
			};
		</script>

		<script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>

		<script src="{{ asset('assets/js/pages/custom/login/login.js') }}" type="text/javascript"></script>

	</body>

</html>