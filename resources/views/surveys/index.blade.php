@extends('layouts.app')

@section('page_styles')

<link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
<style>
    .select2 {
        width: 100% !important;
    }

    .cards tbody tr {
        box-shadow: 0px 0px 13px 0px rgba(82, 63, 105, 0.05);
        background-color: #ffffff;
        margin-bottom: 3px;
        border-radius: 10px;
        display: flex;
        /* flex-grow: 1; */
        flex-direction: column;

        
        float: left;
        width: 32%;
        margin: 0.6rem;
        border: 0.0625rem solid #f2f3f7;
        border-radius: .25rem;
    }

    .kt-widget.kt-widget--project-1 .kt-widget__footer {
        width: 95% !important;
    }

    @media (min-width: 1025px) {
        .cards tbody tr{
            height: calc(100% - 20px);
        }
    }

    @media only screen and (max-width: 992px) {
        .cards tbody tr{
            width: 47.3%;
            display: flex;
        }
    }

    @media only screen and (max-width: 600px) {
        .cards tbody tr{
            width: 100%;
            display: flex;
        }
    }

    .cards thead {
        display: none;
    }

    .table td {
        border-top: 0 !important;
    }

    .kt-widget.kt-widget--project-1 .kt-widget__footer {
        border-top: 1px solid #ebedf2 !important;
    }
</style>

@endsection


@section('content')
	<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

		<div class="kt-subheader  kt-grid__item" id="kt_subheader">
			<div class="kt-container  kt-container--fluid ">
				<div class="kt-subheader__main">
					<h3 class="kt-subheader__title">Surveys</h3>
					{{-- <span class="kt-subheader__separator kt-subheader__separator--v"></span> --}}
					{{-- <span class="kt-subheader__desc">#XRS-45670</span> --}}
					{{-- <a href="#" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
						Add New
					</a> --}}
				</div>
			</div>
        </div>      
        
		<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand flaticon2-line-chart"></i>
						</span>
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-wrapper">
							<div class="kt-portlet__head-actions">
                                <div class="dropdown dropdown-inline">
                                    <button type="button" class="btn btn-brand btn-icon-sm" data-toggle="modal" data-target="#createSurvey">
                                        <i class="flaticon2-plus"></i> Create Survey
                                    </button>
                                </div>
                                <div class="modal fade" id="createSurvey" tabindex="-1" role="dialog" aria-labelledby="createSurveyModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="createSurveyModalLabel">New Survey</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="kt-form">
                                                    <div class="kt-portlet__body">
                                                        <div class="form-group">
                                                            <label>Survey Name <span class="red">*</span> </label>
                                                            <input id="survey_title" type="text" class="form-control" placeholder="Survey Name">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="survey_category">Survey Type <span class="red">*</span></label>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <select class="form-control load-survey-types select2" id="survey_category">
                                                                        <option value="0">-- SELECT TYPE --</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="kt-portlet__foot">
                                                        <div class="kt-form__actions float-right">
                                                            <button data-url="{{ url('surveys/create') }}" id="create_survey" type="button" class="btn btn-primary">Submit</button>
                                                            <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">

					<!--begin: Datatable -->
					<table class="table table-striped table-bordered table-hover kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_table_2">
						<thead>
						   <tr>
							  <th></th>
							  <th></th>
							  <th></th>
						   </tr>
						</thead>
					</table>

					<!--end: Datatable -->
				</div>
			</div>
		</div>

	</div>
@endsection

@section('page_scripts')

{{-- <script src="{{ asset('assets/js/pages/surveys.js') }}" type="text/javascript"></script> --}}
<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/surveys/index.js') }}" type="text/javascript"></script>

@endsection
