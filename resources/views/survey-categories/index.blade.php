@extends('layouts.app')

@section('page_styles')

<link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />

@endsection


@section('content')
	<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

		<div class="kt-subheader  kt-grid__item" id="kt_subheader">
			<div class="kt-container  kt-container--fluid ">
				<div class="kt-subheader__main">
					<h3 class="kt-subheader__title">Survey Categories</h3>
					{{-- <span class="kt-subheader__separator kt-subheader__separator--v"></span> --}}
					{{-- <span class="kt-subheader__desc">#XRS-45670</span> --}}
					{{-- <a href="#" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
						Add New
					</a> --}}
                </div>
			</div>
        </div>
        
		<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand flaticon2-line-chart"></i>
						</span>
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-wrapper">
							<div class="kt-portlet__head-actions">
								<button class="btn btn-brand btn-elevate btn-icon-sm create-sc-btn" data-check='1' data-type="new" data-checkurl="{{ url('survey-categories/check-unsaved-exists') }}" data-url="{{ url('survey-categories/create') }}">
									<i class="la la-plus"></i>
									New Survey Category
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">

					<!--begin: Datatable -->
					<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
						<thead>
							<tr>
								<th>Title</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
					</table>

					<!--end: Datatable -->
				</div>
			</div>
		</div>

	</div>

	<div class="modal fade" id="unsaved_survey_category" tabindex="-1" role="dialog" aria-labelledby="unsavedSurveyCategoryLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="unsavedSurveyCategoryLabel">Unsaved Survey Category</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
					<div class="text-center">
						Looks like there is an unfinished survey category. Do you want to continue?
					</div>
					<div class="text-center mt-3">
						<button class="btn btn-brand btn-elevate btn-icon-sm create-btn m-2" data-type="continue" data-url="{{ url('survey-categories/create') }}">Yes</button>
						<button class="btn btn-brand btn-elevate btn-icon-sm create-btn m-2" data-type="new" data-url="{{ url('survey-categories/create') }}">No. Create New</button>
						<button data-dismiss="modal" class="btn btn-secondary btn-elevate btn-icon-sm m-2">Cancel</button>
					</div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_scripts')

<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/survey-categories/index.js') }}" type="text/javascript"></script>

@endsection
