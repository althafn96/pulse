@extends('layouts.app')

@section('page_styles')

<link href="{{ asset('assets/css/pages/wizard/wizard-1.css') }}" rel="stylesheet" type="text/css" />
<style>
    .select2 {
        width: 100% !important;
    }

    .kt-widget2__item {
        cursor: all-scroll;
    }
    .kt-widget2__item:focus {
        cursor: all-scroll;
    }

    .kt-widget2 .kt-widget2__item {
        padding-bottom: 1.4rem;
        border-bottom: 0.5px solid #f2f3f7;
    }
</style>

@endsection


@section('content')
	<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

		<div class="kt-subheader  kt-grid__item" id="kt_subheader">
			<div class="kt-container  kt-container--fluid ">
				<div class="kt-subheader__main">
					<h3 class="kt-subheader__title">Create Survey Category</h3>
					<span class="kt-subheader__separator kt-subheader__separator--v"></span>
					<span class="kt-subheader__desc">add a new survey category</span>
					{{-- <a href="#" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10 btn-back float-right">
						Back
					</a> --}}
				</div>
                <div class="kt-subheader__toolbar">
                    <div class="kt-subheader__wrapper">
                        <a href="{{ route('survey-categories.index') }}" data-href="" class="btn kt-subheader__btn-primary back-btn">
                            Back

                            <!--<i class="flaticon2-calendar-1"></i>-->
                        </a>
                        <div class="btn-group">
                            <button type="button" class="btn btn-brand btn-bold store-item-btn" data-url="{{url('survey-categories')}}" id="store-item-btn">
                                Submit 
                            </button>
                            <button type="button" class="btn btn-brand btn-bold dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-writing"></i>
                                            <span class="kt-nav__link-text">Save &amp; create Survey</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-medical-records"></i>
                                            <span class="kt-nav__link-text">Save &amp; add new</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-copy"></i>
                                            <span class="kt-nav__link-text">Save &amp; duplicate</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-hourglass-1"></i>
                                            <span class="kt-nav__link-text">Save &amp; exit</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
        </div>
        
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="kt-grid  kt-wizard-v1 kt-wizard-v1--white" id="kt_projects_add" data-ktwizard-state="step-first">
                        <div class="kt-grid__item">

                            <!--begin: Form Wizard Nav -->
                            <div class="kt-wizard-v1__nav">
                                <div class="kt-wizard-v1__nav-items">

                                    <!--doc: Replace A tag with SPAN tag to disable the step link click -->
                                    <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--xl">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <path d="M4.875,20.75 C4.63541667,20.75 4.39583333,20.6541667 4.20416667,20.4625 L2.2875,18.5458333 C1.90416667,18.1625 1.90416667,17.5875 2.2875,17.2041667 C2.67083333,16.8208333 3.29375,16.8208333 3.62916667,17.2041667 L4.875,18.45 L8.0375,15.2875 C8.42083333,14.9041667 8.99583333,14.9041667 9.37916667,15.2875 C9.7625,15.6708333 9.7625,16.2458333 9.37916667,16.6291667 L5.54583333,20.4625 C5.35416667,20.6541667 5.11458333,20.75 4.875,20.75 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                        <path d="M2,11.8650466 L2,6 C2,4.34314575 3.34314575,3 5,3 L19,3 C20.6568542,3 22,4.34314575 22,6 L22,15 C22,15.0032706 21.9999948,15.0065399 21.9999843,15.009808 L22.0249378,15 L22.0249378,19.5857864 C22.0249378,20.1380712 21.5772226,20.5857864 21.0249378,20.5857864 C20.7597213,20.5857864 20.5053674,20.4804296 20.317831,20.2928932 L18.0249378,18 L12.9835977,18 C12.7263047,14.0909841 9.47412135,11 5.5,11 C4.23590829,11 3.04485894,11.3127315 2,11.8650466 Z M6,7 C5.44771525,7 5,7.44771525 5,8 C5,8.55228475 5.44771525,9 6,9 L15,9 C15.5522847,9 16,8.55228475 16,8 C16,7.44771525 15.5522847,7 15,7 L6,7 Z" fill="#000000" />
                                                    </g>
                                                </svg> </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                Survey Category Details
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--xl">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <path d="M11,20 L11,17 C11,16.4477153 11.4477153,16 12,16 C12.5522847,16 13,16.4477153 13,17 L13,20 L15.5,20 C15.7761424,20 16,20.2238576 16,20.5 C16,20.7761424 15.7761424,21 15.5,21 L8.5,21 C8.22385763,21 8,20.7761424 8,20.5 C8,20.2238576 8.22385763,20 8.5,20 L11,20 Z" fill="#000000" opacity="0.3" />
                                                        <path d="M3,5 L21,5 C21.5522847,5 22,5.44771525 22,6 L22,16 C22,16.5522847 21.5522847,17 21,17 L3,17 C2.44771525,17 2,16.5522847 2,16 L2,6 C2,5.44771525 2.44771525,5 3,5 Z M4.5,8 C4.22385763,8 4,8.22385763 4,8.5 C4,8.77614237 4.22385763,9 4.5,9 L13.5,9 C13.7761424,9 14,8.77614237 14,8.5 C14,8.22385763 13.7761424,8 13.5,8 L4.5,8 Z M4.5,10 C4.22385763,10 4,10.2238576 4,10.5 C4,10.7761424 4.22385763,11 4.5,11 L7.5,11 C7.77614237,11 8,10.7761424 8,10.5 C8,10.2238576 7.77614237,10 7.5,10 L4.5,10 Z" fill="#000000" />
                                                    </g>
                                                </svg> </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                Attributes
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--xl">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <path d="M13,18.9450712 L13,20 L14,20 C15.1045695,20 16,20.8954305 16,22 L8,22 C8,20.8954305 8.8954305,20 10,20 L11,20 L11,18.9448245 C9.02872877,18.7261967 7.20827378,17.866394 5.79372555,16.5182701 L4.73856106,17.6741866 C4.36621808,18.0820826 3.73370941,18.110904 3.32581341,17.7385611 C2.9179174,17.3662181 2.88909597,16.7337094 3.26143894,16.3258134 L5.04940685,14.367122 C5.46150313,13.9156769 6.17860937,13.9363085 6.56406875,14.4106998 C7.88623094,16.037907 9.86320756,17 12,17 C15.8659932,17 19,13.8659932 19,10 C19,7.73468744 17.9175842,5.65198725 16.1214335,4.34123851 C15.6753081,4.01567657 15.5775721,3.39010038 15.903134,2.94397499 C16.228696,2.49784959 16.8542722,2.4001136 17.3003976,2.72567554 C19.6071362,4.40902808 21,7.08906798 21,10 C21,14.6325537 17.4999505,18.4476269 13,18.9450712 Z" fill="#000000" fill-rule="nonzero" />
                                                        <circle fill="#000000" opacity="0.3" cx="12" cy="10" r="6" />
                                                    </g>
                                                </svg> </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                Delivery Details
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--xl">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <path d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z" fill="#000000" />
                                                        <circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5" />
                                                    </g>
                                                </svg> </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                Review and Submit
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>

                            <!--end: Form Wizard Nav -->
                        </div>
                        <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v1__wrapper">

                            <!--begin: Form Wizard Form-->
                            <form class="kt-form" id="survey_categories_add_form">

                                <!--begin: Form Wizard Step 1-->
                                <div class="kt-wizard-v1__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                    <div class="kt-heading kt-heading--md">Survey Category Details:</div>
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-wizard-v1__form">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="kt-section__body">
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Category Title *</label>
                                                            <div class="col-lg-9 col-xl-9">
                                                                <input data-url="add-temp-title" value="{{ $temp_survey_category == null ? '' : $temp_survey_category->title }}" required id="sc_title" class="form-control sc_field" type="text" placeholder="Enter Title" name="title">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Form Wizard Step 1-->

                                <!--begin: Form Wizard Step 2-->
                                <div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-wizard-v1__form">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="kt-section__body">
                                                        <div class="form-group row">
                                                            <div class="col-lg-9 col-xl-6">
                                                                <h3 class="kt-section__title kt-section__title-md">Attributes:</h3>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-2 col-lg-2 col-form-label">Attribute Title *</label>
                                                            <div class="col-lg-4 col-xl-4">
                                                                <input id="attr_title" class="form-control sc_field" type="text" placeholder="Enter Title" name="attr_title">
                                                            </div>
                                                            <label class="col-xl-2 col-lg-2 col-form-label float-right" for="attr_field">Attribute Field</label>
                                                            <div class="col-lg-4 col-xl-4">
                                                                <select data-live-search="true" class="form-control load-survey-category-attribute-fields" id="attr_field">
                                                                    @foreach($attribute_fields as $field)
                                                                        <option id="id_selected_{{ $field->code }}" data-value="{{ $field->title }}" value="{{ $field->code }}" data-icon="{{ $field->icon }}">{{ $field->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <label style="display:none" class="col-xl-2 col-lg-2 col-form-label mt-3 options_section">Options*</label>
                                                            <div style="display:none" class="col-lg-10 col-xl-10 mt-3 options_section">
                                                                <div class="kt_repeater_1" id="kt_repeater_1">
                                                                    <div class="kt_repeater_1" id="kt_repeater_1">
                                                                        
                                                                        <div data-repeater-list="">
                                                                            <div data-repeater-item class="mb-3 row align-items-center">
                                                                                <div class="col-lg-5">
                                                                                    <input id="attr_options" class="form-control sc_field" type="text" placeholder="Enter Option" name="attr_options">
                                                                                </div>
                                                                                <div class="col-md-5">
                                                                                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                                                        <i class="la la-trash-o"></i>
                                                                                        Delete
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group form-group-last row">
                                                                        <div class="col-lg-4">
                                                                            <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                                                                <i class="la la-plus"></i> Add
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="col-lg-3 float-right mt-3 pr-0">
                                                                    <button data-url="{{ url('survey-categories/add-temp-attribute') }}" class="form-control btn btn-brand btn-md" type="button" id="add_attribute_to_sc">Add Attribute</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        {{-- <div class="kt-portlet__body kt-portlet__body--fit">

                                                            <!--begin: Datatable -->
                                                            <div class="kt-datatable" id="ajax_data"></div>
                                        
                                                            <!--end: Datatable -->
                                                        </div> --}}
                                                        <div id="attr_list" data-attrListCount="{{ $existing_survey_attributes->count() }}" class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                                                            <div style="padding-bottom: 0 !important" class="kt-portlet__body">
                                                                <div id="sc_attr_list" class="kt-widget2 sc_attr_list" data-edit="edit-temp-attr" data-update="update-temp-attr" data-remove="remove-temp-attr">
                                                                    @foreach($existing_survey_attributes as $sc_attr)
                                                                        <div data-id="{{ $sc_attr->id }}" class="kt-widget2__item kt-widget2__item--brand attr-item-{{ $sc_attr->id }}">
                                                                            <div class="kt-widget2__checkbox">
                                                                                
                                                                            </div>
                                                                            <div class="kt-widget2__info">
                                                                                <a href="#" onclick="return false;" class="kt-widget2__title">
                                                                                    {{ $sc_attr->title }}
                                                                                </a>
                                                                                <a href="#" onclick="return false;" class="kt-widget2__username">
                                                                                    {{ $sc_attr->attr_title }}
                                                                                </a>
                                                                                <a href="#" onclick="return false;" class="kt-widget2__username">
                                                                                    {{ $sc_attr->options }}
                                                                                </a>
                                                                            </div>
                                                                            <div class="kt-widget2__actions">
                                                                                <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                                                                    <i class="flaticon-more-1"></i>
                                                                                </a>
                                                                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                                                                    <ul class="kt-nav">
                                                                                        <li class="kt-nav__item">
                                                                                            <a data-id="{{ $sc_attr->id }}" class="kt-nav__link edit-item">
                                                                                                <i class="kt-nav__link-icon flaticon2-gear"></i>
                                                                                                <span class="kt-nav__link-text">Edit</span>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li class="kt-nav__item">
                                                                                            <a data-id="{{ $sc_attr->id }}" class="kt-nav__link remove-item">
                                                                                                <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                                                                <span class="kt-nav__link-text">Remove</span>
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Form Wizard Step 2-->

                                <!--begin: Form Actions -->
                                <div class="kt-form__actions">
                                    <div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-prev">
                                        Previous
                                    </div>
                                    {{-- <div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
                                        Submit
                                    </div> --}}
                                    <div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-next">
                                        Next Step
                                    </div>
                                </div>

                                <!--end: Form Actions -->
                            </form>

                            <!--end: Form Wizard Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

	</div>
@endsection

@section('page_scripts')

<script src="http://SortableJS.github.io/Sortable/Sortable.js"></script>
<script>
    "use strict";

    // Class definition
    var KTProjectsAdd = function () {
        // Base elements
        var wizardEl;
        var formEl;
        var validator;
        var wizard;
        var avatar;

        // Private functions
        var initWizard = function () {
            // Initialize form wizard
            wizard = new KTWizard('kt_projects_add', {
                startStep: 1, // initial active step number
                clickableSteps: true  // allow step clicking
            });

            // Validation before going to next page
            wizard.on('beforeNext', function(wizardObj) {
                if (validator.form() !== true) {
                    wizardObj.stop();  // don't go to the next step
                }
            })

            // Change event
            wizard.on('change', function(wizard) {
                KTUtil.scrollTop();
            });
        }

        var initValidation = function() {
            validator = formEl.validate({
                // Validate only visible fields
                ignore: ":hidden",

                // Validation rules
                rules: {
                    // Step 1
                    profile_avatar: {
                        //required: true
                    },
                    profile_first_name: {
                        required: true
                    },
                    profile_last_name: {
                        required: true
                    },
                    profile_phone: {
                        required: true
                    },
                    profile_email: {
                        required: true,
                        email: true
                    }
                },

                // Display error
                invalidHandler: function(event, validator) {
                    KTUtil.scrollTop();
                    $.notify(
                        {
                            // options
                            icon: "la la-warning",
                            title: 'Error',
                            message: 'required fields cannot be empty'
                        },
                        {
                            type: "error-type",
                            placement: {
                                from: "bottom",
                                align: "left"
                            },
                            animate: {
                                enter: "animated slideInLeft",
                                exit: "animated slideOutLeft"
                            },
                            icon_type: "class",
                            template:
                                '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss"></button>' +
                                '<div class="row">' +
                                '<div class="col-md-1"><span data-notify="icon"></span></div>' +
                                '<div class="col-md-11"><span data-notify="title">{1}</span></div>' +
                                '<div class="col-md-11 offset-md-1"><span data-notify="message">{2}</span></div>' +
                                "</div></div>"
                        }
                    );
                },

                // Submit valid form
                submitHandler: function (form) {

                }
            });
        }

        var initSubmit = function() {
            var btn = formEl.find('[data-ktwizard-type="action-submit"]');

            btn.on('click', function(e) {
                e.preventDefault();

                if (validator.form()) {
                    // See: src\js\framework\base\app.js
                    KTApp.progress(btn);
                    //KTApp.block(formEl);

                    // See: http://malsup.com/jquery/form/#ajaxSubmit
                    formEl.ajaxSubmit({
                        success: function() {
                            KTApp.unprogress(btn);
                            //KTApp.unblock(formEl);

                            swal.fire({
                                "title": "",
                                "text": "The application has been successfully submitted!",
                                "type": "success",
                                "confirmButtonClass": "btn btn-secondary"
                            });
                        }
                    });
                }
            });
        }

        var initAvatar = function() {
            avatar = new KTAvatar('kt_projects_add_avatar');
        }

        return {
            // public functions
            init: function() {
                formEl = $('#survey_categories_add_form');

                initWizard();
                initValidation();
                initSubmit();
                initAvatar();
            }
        };
    }();

    jQuery(document).ready(function() {
        KTProjectsAdd.init();
    });

</script>
<script src="{{ asset('assets/js/survey-categories/create-edit.js') }}" type="text/javascript"></script>
@endsection
