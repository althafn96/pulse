var SurveysTable = function () {
    // Private functions

    // basic survey_table
    var survey_table = function () {

        var datatable = $('.kt-datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: 'survey-categories/get-attributes',
                        method: 'get',
                        // sample custom headers
                        // headers: {'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // layout definition
            layout: {
                scroll: false,
                footer: false,
            },

            // column sorting
            sortable: true,

            pagination: true,

            // columns definition
            columns: [
                {
                    field: 'Title',
                    title: 'Title',
                }, {
                    field: 'Attribute',
                    title: 'Attribute',
                }, {
                    field: 'Options',
                    title: 'Options',
                }, {
                    field: 'Actions',
                    title: 'Actions',
                    sortable: false,
                    width: 110,
                    overflow: 'visible',
                    autoHide: false,
                    template: function () {
                        return '\
                        <div class="dropdown">\
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm" data-toggle="dropdown">\
                                <i class="flaticon2-gear"></i>\
                            </a>\
                            <div class="dropdown-menu dropdown-menu-right">\
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>\
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>\
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>\
                            </div>\
                        </div>\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Edit details">\
                            <i class="flaticon2-paper"></i>\
                        </a>\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Delete">\
                            <i class="flaticon2-trash"></i>\
                        </a>\
                    ';
                    },
                }],

        });

    };

    return {
        // public functions
        init: function () {
            survey_table();
        },
    };
}();

jQuery(document).ready(function () {
    SurveysTable.init();

    $('.load-survey-category-attribute-fields').selectpicker();

    if ($('#attr_list').attr('data-attrListCount') == '0') {
        $('#attr_list').css('display', 'none');
    }
});

$('.kt_repeater_1').repeater({
    initEmpty: false,

    defaultValues: {
        'text-input': 'foo'
    },

    show: function () {
        $(this).slideDown();
    },

    hide: function (deleteElement) {
        $(this).slideUp(deleteElement);
    }
});

$('#sc_title').keyup(function () {
    sc_title = $(this).val();

    $.ajax({
        type: "GET",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: 'add-temp-sc-title',
        data: 'sc_title=' + sc_title,
        dataType: "json",
        success: function (response) {
            $('#sc_title').attr('data-temp-sc', response.id);
        },
        error: function (response) {

            if (response.status == '401') {
                notify_toast('la la-warning', 'Warning', 'warning-type', 'session expired. please login to continue');

                setTimeout(() => {
                    location.reload();
                }, 1000);
            }
        }
    });
});

$('#attr_field').change(function () {
    attr_code = $('#attr_field').val();

    if (attr_code == '002' || attr_code == '003' || attr_code == '004') {
        $('.options_section').removeAttr('style');
    } else {

        $('input[name*="attr_options"]').each(function (e) {
            $(this).val('');
        });

        $('.options_section').css('display', 'none');
    }

});


Sortable.create(sc_attr_list, {
    scroll: true,
    store: {
        set: function (sortable) {
            // let order = sortable.toArray();
            // console.log(order);

            let order = {};
            $('.kt-widget2__item').each(function () {
                order[$(this).data('id')] = $(this).index();
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: 'change-temp-attr-order',
                type: 'POST',
                data: order,
                success: function (response) {
                    console.log(response)
                },
                error: function (response) {

                    if (response.status == '401') {
                        notify_toast('la la-warning', 'Warning', 'warning-type', 'session expired. please login to continue');

                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    }
                }
            })
        }
    }
});

$('#add_attribute_to_sc').click(function () {

    options = [];
    attr_code = $('#attr_field').val();
    btn_id = $(this).attr('id');
    $('#' + btn_id).addClass('kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light');
    $('#' + btn_id).html('Loading');

    if (attr_code == '002' || attr_code == '003' || attr_code == '004') {
        $('input[name*="attr_options"]').each(function (e) {
            if ($(this).val() !== '') {
                options.push($(this).val());
            }
        });
    }

    data = {
        title: $('#attr_title').val(),
        attr: $('#attr_field').val(),
        options: options
    }

    addItem($(this).data('url'), data)
        .then(data => {
            if (data.type == 'Success') {
                notify_toast('la la-check-circle', data.type, 'success-type', data.text);

                $('#attr_title').val('');
                $('input[name*="attr_options"]').each(function (e) {
                    $(this).val('');
                });

                attr_list_res = data.data;

                attr_list = '';

                attr_list_res.forEach(attrList);

                if (attr_list == '') {

                    $('#attr_list').css('display', 'none');
                    $('#sc_attr_list').html('');

                } else {

                    attr_list_res.forEach(function () {
                        $('#attr_list').removeAttr('style');

                        $('#sc_attr_list').html(attr_list);
                    });

                }
            } else {
                notify_toast('la la-warning', data.type, 'error-type', data.text);
            }
            $('#' + btn_id).removeClass('kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light');
            $('#' + btn_id).html('Add Attribute');
        })
        .catch(error => {
            notify_toast('la la-warning', 'Warning', 'warning-type', 'unknown error occurred. please contact management');
            $('#' + btn_id).removeClass('kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light');
            $('#' + btn_id).html('Add Attribute');
        });
});

$('#sc_attr_list').on('click', '.remove-item', function () {
    id = $(this).data('id');
    url = $(this).data('url');

    removeItem(id, url)
        .then(data => {
            if (data.type == 'Success') {

                attr_list_res = data.data;

                attr_list = '';

                attr_list_res.forEach(attrList);

                if (attr_list == '') {

                    $('#attr_list').css('display', 'none');
                    $('#sc_attr_list').html('');

                } else {

                    attr_list_res.forEach(function () {
                        $('#attr_list').removeAttr('style');

                        $('#sc_attr_list').html(attr_list);
                    });

                }
            }
        })
        .catch(error => {

        });
});

$('#sc_attr_list').on('click', '.edit-item', function () {
    id = $(this).data('id');
    url = $(this).data('url');

    editItem(id, url)
        .then(data => {
            if (data.type == 'Success') {
                attr_list_res = data.attribute_fields;
                attr_fields_list = '';

                attr_list_res.forEach(function (item, index) {
                    if (item.code == data.data.field_code) {
                        attr_fields_list += `<option id="id_selected_` + item.code + `" data-value="` + item.title + `" selected value="` + item.code + `" data-icon="` + item.icon + `">` + item.title + `</option>`;
                    } else {
                        attr_fields_list += `<option id="id_selected_` + item.code + `" data-value="` + item.title + `" value="` + item.code + `" data-icon="` + item.icon + `">` + item.title + `</option>`;
                    }
                });

                repeater_fields = '';

                if (data.data.field_code == '002' || data.data.field_code == '003' || data.data.field_code == '004') {
                    repeater_fields += '<label class="col-xl-2 col-lg-2 col-form-label mt-3 options_section">Options*</label>';


                    var opt_arr = JSON.parse(data.data.options);
                    opt_arr.forEach(function (item, index) {
                        if (index == 0) {
                            repeater_fields += `
                                <div class="col-lg-10 col-xl-10 mt-3 options_section">
                                    <div class="kt_repeater_2" id="kt_repeater_2">
                                        <div class="kt_repeater_2" id="kt_repeater_2">
                                            
                                            <div data-repeater-list="">
                                                <div data-repeater-item class="mb-3 row align-items-center">
                                                    <div class="col-lg-5">
                                                        <input value="`+ item + `" id="attr_options" class="form-control sc_field attr_options_` + id + `" type="text" name="attr_options">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                            <i class="la la-trash-o"></i>
                                                            Delete
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `;
                        } else {
                            repeater_fields += `
                                <div class="col-lg-10 col-xl-10 offset-lg-2 offset-xl-2 mt-3 options_section">
                                    <div class="kt_repeater_2" id="kt_repeater_2">
                                        <div class="kt_repeater_2" id="kt_repeater_2">
                                            
                                            <div data-repeater-list="">
                                                <div data-repeater-item class="mb-3 row align-items-center">
                                                    <div class="col-lg-5">
                                                        <input value="`+ item + `" id="attr_options" class="form-control sc_field attr_options_` + id + `" type="text" name="attr_options">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                            <i class="la la-trash-o"></i>
                                                            Delete
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `;
                        }

                    });

                    repeater_fields += `
                            <div class="col-lg-10 col-xl-10 mt-3 offset-lg-2 offset-xl-2 options_section">
                                <div class="kt_repeater_2" id="kt_repeater_2">
                                    <div class="kt_repeater_2" id="kt_repeater_2">
                                        
                                        <div data-repeater-list="">
                                            <div data-repeater-item class="mb-3 row align-items-center">
                                                <div class="col-lg-5">
                                                    <input id="attr_options" class="form-control sc_field attr_options_`+ id + `" type="text" placeholder="Enter Option" name="attr_options">
                                                </div>
                                                <div class="col-md-5">
                                                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        Delete
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-last row">
                                        <div class="col-lg-4">
                                            <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                                <i class="la la-plus"></i> Add
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    `;
                }


                attr_edit_form = `
                    <div class="form-group row">
                        <label class="col-xl-2 col-lg-2 col-form-label">Attribute Title *</label>
                        <div class="col-lg-4 col-xl-4">
                            <input id="attr_title_`+ id + `" class="form-control sc_field" type="text" value="` + data.data.title + `" name="attr_title">
                        </div >
                        <label class="col-xl-2 col-lg-2 col-form-label float-right" for="attr_field_`+ id + `">Attribute Field</label>
                        <div class="col-lg-4 col-xl-4">
                            <select data-live-search="true" class="form-control load-survey-category-attribute-fields" id="attr_field_`+ id + `">
                                `+ attr_fields_list + `
                                </select>
                        </div>
                        `+ repeater_fields + `
                        <div class="col-lg-12 mt-3">
                            <div class="col-lg-2 float-right pr-0">
                                <button id="update-`+ id + `_btn" data-id="` + data.data.id + `" data-url="update-attribute" class="form-control btn btn-brand btn-md update_attribute_to_sc" type="button">Update</button>
                            </div>
                            <div class="col-lg-2 float-right pr-0">
                                <input type="hidden" id="attr_options_` + data.data.id + `" value='` + data.data.options + `' >
                                <button id="cancel-update-` + id + `_btn" data-id="` + data.data.id + `"class="form-control btn btn-secondary btn-md cancel_update_attribute_to_sc" type="button">Cancel</button>
                            </div>
                        </div>
                    </div >
                    `;
                $('.attr-item-' + id).removeClass('kt-widget2__item kt-widget2__item--brand');
                $('.attr-item-' + id).css('borderBottom', '0.5px solid #f2f3f7');
                $('.attr-item-' + id).css('marginBottom', '1.4rem');
                $('.attr-item-' + id).html(attr_edit_form);
                $('.load-survey-category-attribute-fields').selectpicker();

                $('.kt_repeater_2').repeater({
                    initEmpty: false,

                    defaultValues: {
                        'text-input': 'foo'
                    },

                    show: function () {
                        $(this).slideDown();
                    },

                    hide: function (deleteElement) {
                        $(this).slideUp(deleteElement);
                    }
                });
            }
        })
        .catch(error => {

        });
});

function attrList(item, index) {
    attr_list += `
                    <div data-id="`+ item.id + `" class="kt-widget2__item kt-widget2__item--brand attr-item-` + item.id + `" >
                        <div class="kt-widget2__checkbox">

                        </div>
                        <div class="kt-widget2__info">
                            <a href="#" onclick="return false;" class="kt-widget2__title">
                                `+ item.title + `
                            </a>
                            <a href="#" onclick="return false;" class="kt-widget2__username">
                                `+ item.attr_title + `
                            </a>
                            <a href="#" onclick="return false;" class="kt-widget2__username">
                                `+ item.options + `
                            </a>
                        </div>
                        <div class="kt-widget2__actions">
                            <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                <i class="flaticon-more-1"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a data-id="`+ item.id + `" data-url="edit-temp-attr" class="kt-nav__link edit-item">
                                            <i class="kt-nav__link-icon flaticon2-gear"></i>
                                            <span class="kt-nav__link-text">Edit</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a data-id="`+ item.id + `" data-url="remove-temp-attr" class="kt-nav__link remove-item">
                                            <i class="kt-nav__link-icon flaticon2-trash"></i>
                                            <span class="kt-nav__link-text">Remove</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div >
                    `;
}

$('#sc_attr_list').on('click', '.update_attribute_to_sc', function () {
    id = $(this).data('id');
    url = $(this).data('url');
    btn_id = $(this).attr('id');

    options = [];


    $('.attr_options_' + id).each(function (e) {
        if ($(this).val() !== '') {
            options.push($(this).val());
        }
    });

    data = {
        id: id,
        title: $('#attr_title_' + id).val(),
        attr: $('#attr_field_' + id).val(),
        options: options
    }

    updateItem(btn_id, id, url, data, redirect_url = null)
        .then(data => {
            if (data.type == 'Success') {
                attr_fields_list = '';

                attr_updated_field = `
                        <div class="kt-widget2__checkbox">

                        </div>
                        <div class="kt-widget2__info">
                            <a href="#" onclick="return false;" class="kt-widget2__title">
                                `+ data.data.title + `
                            </a>
                            <a href="#" onclick="return false;" class="kt-widget2__username">
                                `+ data.data.attr_title + `
                            </a>
                            <a href="#" onclick="return false;" class="kt-widget2__username">
                                `+ data.data.options + `
                            </a>
                        </div>
                        <div class="kt-widget2__actions">
                            <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                <i class="flaticon-more-1"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a data-id="`+ data.data.id + `" data-url="edit-temp-attr" class="kt-nav__link edit-item">
                                            <i class="kt-nav__link-icon flaticon2-gear"></i>
                                            <span class="kt-nav__link-text">Edit</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a data-id="`+ data.data.id + `" data-url="remove-temp-attr" class="kt-nav__link remove-item">
                                            <i class="kt-nav__link-icon flaticon2-trash"></i>
                                            <span class="kt-nav__link-text">Remove</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    `;
                $('.attr-item-' + id).addClass('kt-widget2__item kt-widget2__item--brand');
                $('.attr-item-' + id).html(attr_updated_field);
                $('.load-survey-category-attribute-fields').selectpicker();
            }
        })
        .catch(error => {

        });
});

$('#sc_attr_list').on('click', '.cancel_update_attribute_to_sc', function () {
    id = $(this).data('id');
    btn_id = $(this).attr('id');

    attr_value = $('#attr_field_' + id).val();
    options = $('#attr_options_' + id).val();

    data = {
        id: id,
        title: $('#attr_title_' + id).val(),
        attr: $('#id_selected_' + attr_value).data('value'),
        options: options
    }

    attr_fields_list = '';

    attr_updated_field = `
                        <div class="kt-widget2__checkbox">

                        </div>
                        <div class="kt-widget2__info">
                            <a href="#" onclick="return false;" class="kt-widget2__title">
                                `+ data.title + `
                            </a>
                            <a href="#" onclick="return false;" class="kt-widget2__username">
                                `+ data.attr + `
                            </a>
                            <a href="#" onclick="return false;" class="kt-widget2__username">
                                `+ data.options + `
                            </a>
                        </div>
                        <div class="kt-widget2__actions">
                            <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                <i class="flaticon-more-1"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a data-id="`+ data.id + `" data-url="edit-temp-attr" class="kt-nav__link edit-item">
                                            <i class="kt-nav__link-icon flaticon2-gear"></i>
                                            <span class="kt-nav__link-text">Edit</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a data-id="`+ data.id + `" data-url="remove-temp-attr" class="kt-nav__link remove-item">
                                            <i class="kt-nav__link-icon flaticon2-trash"></i>
                                            <span class="kt-nav__link-text">Remove</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    `;
    $('.attr-item-' + id).addClass('kt-widget2__item kt-widget2__item--brand');
    $('.attr-item-' + id).html(attr_updated_field);
    $('.load-survey-category-attribute-fields').selectpicker();
});