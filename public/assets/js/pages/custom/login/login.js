$("#kt_login_signin_submit").click(function (e) {
    e.preventDefault();

    $("#kt_login_signin_submit").addClass(
        "kt-spinner kt-spinner--sm kt-spinner--danger"
    );
    $("#kt_login_signin_submit").html("Loading");
    $("#kt_login_signin_submit").attr("disabled", "true");

    url = $(this).attr("dataUrl");

    var data = {
        email: $("#email").val(),
        password: $("#password").val()
    };

    $.ajax({
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: url + "/login",
        dataType: "json",
        data: data,
        success: function (response) {
            if (response.type === "Error") {
                $.notify(
                    {
                        // options
                        icon: "la la-warning",
                        title: response.type,
                        message: response.text
                    },
                    {
                        type: "error-type",
                        placement: {
                            from: "bottom",
                            align: "left"
                        },
                        animate: {
                            enter: "animated slideInLeft",
                            exit: "animated slideOutLeft"
                        },
                        icon_type: "class",
                        template:
                            '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss"></button>' +
                            '<div class="row">' +
                            '<div class="col-md-1"><span data-notify="icon"></span></div>' +
                            '<div class="col-md-11"><span data-notify="title">{1}</span></div>' +
                            '<div class="col-md-11 offset-md-1"><span data-notify="message">{2}</span></div>' +
                            "</div></div>"
                    }
                );

                $("#kt_login_signin_submit").removeClass(
                    "kt-spinner kt-spinner--sm kt-spinner--danger"
                );
                $("#kt_login_signin_submit").html("Sign In");
                $("#kt_login_signin_submit").removeAttr("disabled");
            } else if (response.type === "Success") {
                $.notify(
                    {
                        // options
                        icon: "la la-check-circle",
                        title: response.type,
                        message: response.text
                    },
                    {
                        type: "success-type",
                        placement: {
                            from: "bottom",
                            align: "left"
                        },
                        animate: {
                            enter: "animated slideInLeft",
                            exit: "animated slideOutLeft"
                        },
                        icon_type: "class",
                        template:
                            '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss"></button>' +
                            '<div class="row">' +
                            '<div class="col-md-1"><span data-notify="icon"></span></div>' +
                            '<div class="col-md-11"><span data-notify="title">{1}</span></div>' +
                            '<div class="col-md-11 offset-md-1"><span data-notify="message">{2}</span></div>' +
                            "</div></div>"
                    }
                );

                setTimeout(() => {
                    location.reload();
                }, 1000);
            }
        },
        error: function (response) { }
    });
});
