pulseDtTable = $("#kt_table_1").DataTable({
    responsive: true,
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: "participants",
    language: {
        processing: `
                <div class="indicator"> 
                    <svg width="16px" height="12px">
                        <polyline id="back" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
                        <polyline id="front" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
                    </svg>
                </div>
                `,
    },
    columns: [
        { data: "name", name: "name" },
        { data: "group", name: "group" },
        { data: "status", name: "status" },
        { data: "actions", responsivePriority: -1 },
    ],
    columnDefs: [
        {
            targets: -1,
            width: "100px",
            title: "Actions",
            orderable: false,
            render: function (data, type, full, meta) {
                return (
                    `
                        <a href="participants/` +
                    full.id +
                    `/edit" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Edit Participant Group">							
                            <i class="flaticon2-gear"></i>						
                        </a>
                        <a  onclick="removeDtItem('` +
                    full.id +
                    `','participants/` +
                    full.id +
                    `')" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Remove Participant Group">							
                            <i class="flaticon2-rubbish-bin"></i>						
                        </a>
                        `
                );
            },
        },
        {
            targets: -2,
            render: function (data, type, full, meta) {
                var status = {
                    0: {
                        title: "Disabled",
                        class: "kt-badge--danger",
                        action: "Enable",
                        change: "1",
                    },
                    1: {
                        title: "Enabled",
                        class: " kt-badge--success",
                        action: "Disable",
                        change: "0",
                    },
                };
                if (typeof status[data] === "undefined") {
                    return data;
                }
                return (
                    `
                            <div class="kt-badge ` +
                    status[data].class +
                    ` kt-badge--inline kt-badge--pill">
                                <a class="dropdown-toggle" style="color: white" data-toggle="dropdown" href="#">` +
                    status[data].title +
                    `</a>
                                <div class="dropdown-menu">
                                    <button class="dropdown-item" onclick="change_status('` +
                    status[data].change +
                    `', '` +
                    full.id +
                    `', 'participants/change-status')">` +
                    status[data].action +
                    `</button>
                                </div>
                            </div>
                        `
                );
            },
        },
    ],
});
