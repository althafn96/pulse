"use strict";

// Class definition
var KTProjectsAdd = (function () {
    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var wizard;
    var avatar;

    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard("kt_projects_add", {
            startStep: 1, // initial active step number
            clickableSteps: true, // allow step clicking
        });

        // Validation before going to next page
        wizard.on("beforeNext", function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
        });

        // Change event
        wizard.on("change", function (wizard) {
            KTUtil.scrollTop();
        });
    };

    var initValidation = function () {
        validator = formEl.validate({
            // Validate only visible fields
            // ignore: ":hidden",

            // Validation rules
            rules: {
                // Step 1
                participant_title: {
                    required: true,
                },
                participant_fname: {
                    required: true,
                },
                participant_lname: {
                    required: true,
                },
                participant_group: {
                    required: true,
                },
                participant_email: {
                    required: true,
                    email: true,
                },
            },

            // Display error
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
                $.notify(
                    {
                        // options
                        icon: "la la-warning",
                        title: "Error",
                        message: "required fields cannot be empty",
                    },
                    {
                        type: "error-type",
                        placement: {
                            from: "bottom",
                            align: "left",
                        },
                        animate: {
                            enter: "animated slideInLeft",
                            exit: "animated slideOutLeft",
                        },
                        icon_type: "class",
                        template:
                            '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss"></button>' +
                            '<div class="row">' +
                            '<div class="col-md-1"><span data-notify="icon"></span></div>' +
                            '<div class="col-md-11"><span data-notify="title">{1}</span></div>' +
                            '<div class="col-md-11 offset-md-1"><span data-notify="message">{2}</span></div>' +
                            "</div></div>",
                    }
                );
            },
        });
    };

    return {
        // public functions
        init: function () {
            formEl = $("#participants_add_form");

            initWizard();
            initValidation();
        },
    };
})();

jQuery(document).ready(function () {
    KTProjectsAdd.init();

    $(".select2").select2();
});

$("#participant_group").change(function () {
    KTApp.blockPage();
    $.ajax({
        type: "GET",
        url: "get-participant-group-attributes",
        data: "group_id=" + $("#participant_group").val(),
        dataType: "json",
        success: function (response) {
            if (response.type == "Success") {
                console.log(response.data.length);
                if (response.data.length > 0) {
                    var data = response.data;
                    var fields = "";

                    data.forEach(function (item, index) {
                        fields +=
                            `
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">` +
                            item.title +
                            `</label>
                            <div class="col-lg-9 col-xl-9">
                        `;
                        if (item.field_code == "001") {
                            fields +=
                                `
                            <input id="attr-` +
                                item.id +
                                `" class="form-control" type="text" placeholder="Enter ` +
                                item.title +
                                `" name="attr-` +
                                item.id +
                                `">
                            `;
                        } else if (
                            item.field_code == "002" ||
                            item.field_code == "003" ||
                            item.field_code == "004"
                        ) {
                            var options = item.options;
                            options = options.substring(2);
                            options = options.substring(0, options.length - 2);
                            var options_arr = options.split('","');

                            if (item.field_code == "002") {
                                fields += `<div class="kt-radio-list">`;

                                options_arr.forEach(function (
                                    optItem,
                                    optIndex
                                ) {
                                    fields +=
                                        `
                                        <label class="kt-radio">
                                            <input value="` +
                                        optItem +
                                        `" name="attr-` +
                                        item.id +
                                        `" type="radio">` +
                                        optItem +
                                        `
                                            <span></span>
                                        </label>
                                    `;
                                });
                                fields += `</div>`;
                            } else if (item.field_code == "003") {
                                fields += `<div class="kt-checkbox-list">`;

                                options_arr.forEach(function (
                                    optItem,
                                    optIndex
                                ) {
                                    fields +=
                                        `
                                        <label class="kt-checkbox">
                                            <input value="` +
                                        optItem +
                                        `" name="attr-` +
                                        item.id +
                                        `-` +
                                        optItem +
                                        `" type="checkbox">` +
                                        optItem +
                                        `
                                            <span></span>
                                        </label>
                                    `;
                                });
                                fields += `</div>`;
                            } else if (item.field_code == "004") {
                                fields +=
                                    `<select class="select2" name="attr-` +
                                    item.id +
                                    `">
                                    <option value="">--Select ` +
                                    item.title +
                                    `--</option>`;

                                options_arr.forEach(function (
                                    optItem,
                                    optIndex
                                ) {
                                    fields +=
                                        `
                                        <option value="` +
                                        optItem +
                                        `">` +
                                        optItem +
                                        `</option>
                                    `;
                                });
                                fields += ` </select>`;
                            }
                        }
                        fields += `</div></div>`;
                    });
                    $(".additional-details-section").css("display", "");

                    $("#additional-details-fields").html(fields);
                    $(".select2").select2();
                    KTApp.unblockPage();
                }
            } else {
                notify_toast(
                    "la la-warning",
                    "Warning",
                    "warning-type",
                    "unknown error. please try again later"
                );
                KTApp.unblockPage();
            }
        },
        error: function (response) {
            if (response.status == "401") {
                notify_toast(
                    "la la-warning",
                    "Warning",
                    "warning-type",
                    "session expired. please login to continue"
                );

                setTimeout(() => {
                    location.reload();
                }, 2000);
            } else {
                notify_toast(
                    "la la-warning",
                    "Warning",
                    "warning-type",
                    "unknown error. please try again later"
                );
                KTApp.unblockPage();
            }
        },
    });
});

$("#store-item-btn").click(function () {
    $("#store-item-btn")
        .addClass(
            "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
        )
        .html("loading...");

    var url = $(this).data("url");
    var fdata = new FormData($("#participants_add_form")[0]);

    addItem(url, fdata, "formdata")
        .then((data) => {
            if (data.type == "Success") {
                notify_toast(
                    "la la-check-circle",
                    data.type,
                    "success-type",
                    data.text
                );

                setTimeout(() => {
                    window.location.href = $(".back-btn").attr("href");
                }, 2000);
            } else {
                notify_toast(
                    "la la-warning",
                    data.type,
                    "error-type",
                    data.text
                );
                $("#store-item-btn")
                    .removeClass(
                        "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
                    )
                    .html("Submit");
            }
        })
        .catch((error) => {
            notify_toast(
                "la la-warning",
                "Warning",
                "warning-type",
                "unknown error occurred. please contact management"
            );
            $("#store-item-btn")
                .removeClass(
                    "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
                )
                .html("Submit");
        });
});
