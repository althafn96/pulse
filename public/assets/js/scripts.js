$(".create-btn").click(function () {
    btn = $(this);

    $(btn)
        .addClass(
            "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
        )
        .html("loading...");

    $.notify(
        {
            // options
            icon: "la la-flask",
            message: "Hang on while we set up things for you...",
        },
        {
            type: "info-type",
            placement: {
                from: "bottom",
                align: "center",
            },
            animate: {
                enter: "animated slideInLeft",
                exit: "animated slideOutLeft",
            },
            icon_type: "class",
            template:
                '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss"></button>' +
                '<div class="row">' +
                '<div class="col-md-1"><span data-notify="icon"></span></div>' +
                // '<div class="col-md-11"><span data-notify="title">{1}</span></div>' +
                '<div class="col-md-11"><span data-notify="message">{2}</span></div>' +
                "</div></div>",
        }
    );

    setTimeout(() => {
        load_page = $(this).data("url") + "?type=" + $(this).data("type");
        window.location.href = load_page;
    }, 2000);
});

$(".back-btn").click(function () {
    $(".back-btn").addClass(
        "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
    );
    $(".back-btn").html("loading...");
});

function notify_toast(
    icon,
    title,
    type,
    message,
    from = "bottom",
    align = "left",
    enter = "animated slideInLeft",
    exit = "animated slideOutLeft"
) {
    $.notify(
        {
            // options
            icon: icon,
            message: message,
            title: title,
        },
        {
            type: type,
            placement: {
                from: from,
                align: align,
            },
            animate: {
                enter: enter,
                exit: exit,
            },
            icon_type: "class",
            template:
                '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss"></button>' +
                '<div class="row">' +
                '<div class="col-md-1"><span data-notify="icon"></span></div>' +
                '<div class="col-md-11"><span data-notify="title">{1}</span></div>' +
                '<div class="col-md-11"><span data-notify="message">{2}</span></div>' +
                "</div></div>",
        }
    );
}

function addItem(url, data, type = null) {
    return new Promise((resolve, reject) => {
        if (type == "formdata") {
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (response) {
                    resolve(response);
                },
                error: function (response) {
                    if (response.status == "401") {
                        notify_toast(
                            "la la-warning",
                            "Warning",
                            "warning-type",
                            "session expired. please login to continue"
                        );

                        setTimeout(() => {
                            location.reload();
                        }, 2000);
                    } else {
                        reject(response);
                    }
                },
            });
        } else {
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: "json",
                success: function (response) {
                    resolve(response);
                },
                error: function (response) {
                    if (response.status == "401") {
                        notify_toast(
                            "la la-warning",
                            "Warning",
                            "warning-type",
                            "session expired. please login to continue"
                        );

                        setTimeout(() => {
                            location.reload();
                        }, 2000);
                    } else {
                        reject(response);
                    }
                },
            });
        }
    });
}

function removeItem(id, url) {
    data = {
        id: id,
    };

    return new Promise((resolve, reject) => {
        $.ajax({
            type: "DELETE",
            url: url,
            data: data,
            dataType: "json",
            success: function (response) {
                if (response.type == "Success") {
                    notify_toast(
                        "la la-check-circle",
                        response.type,
                        "success-type",
                        response.text
                    );
                } else {
                    notify_toast(
                        "la la-warning",
                        response.type,
                        "error-type",
                        response.text
                    );
                }
                resolve(response);
            },
            error: function (response) {
                if (response.status == "401") {
                    notify_toast(
                        "la la-warning",
                        "Warning",
                        "warning-type",
                        "session expired. please login to continue"
                    );

                    setTimeout(() => {
                        location.reload();
                    }, 1000);
                } else {
                    notify_toast(
                        "la la-warning",
                        "Warning",
                        "warning-type",
                        "unknown error occurred. please contact management"
                    );
                    reject(response);
                }
            },
        });
    });
}

function editItem(id, url) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "GET",
            url: url,
            data: "id=" + id,
            dataType: "json",
            success: function (response) {
                resolve(response);
            },
            error: function (response) {
                if (response.status == "401") {
                    notify_toast(
                        "la la-warning",
                        "Warning",
                        "warning-type",
                        "session expired. please login to continue"
                    );

                    setTimeout(() => {
                        location.reload();
                    }, 1000);
                } else {
                    notify_toast(
                        "la la-warning",
                        "Warning",
                        "warning-type",
                        "unknown error occurred. please contact management"
                    );
                    reject(response);
                }
            },
        });
    });
}

function updateItem(btn_id, url, data, type = null) {
    original_html = $("#" + btn_id).html();

    $("#" + btn_id)
        .addClass(
            "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
        )
        .html("loading...");

    return new Promise((resolve, reject) => {
        if (type == "formdata") {
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (response) {
                    if (response.type == "Success") {
                        notify_toast(
                            "la la-check-circle",
                            response.type,
                            "success-type",
                            response.text
                        );
                    } else {
                        notify_toast(
                            "la la-warning",
                            response.type,
                            "error-type",
                            response.text
                        );
                    }
                    $("#" + btn_id).removeClass(
                        "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
                    );
                    $("#" + btn_id).html(original_html);
                    resolve(response);
                },
                error: function (response) {
                    if (response.status == "401") {
                        notify_toast(
                            "la la-warning",
                            "Warning",
                            "warning-type",
                            "session expired. please login to continue"
                        );

                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    } else {
                        notify_toast(
                            "la la-warning",
                            "Warning",
                            "warning-type",
                            "unknown error occurred. please contact management"
                        );
                        $("#" + btn_id).removeClass(
                            "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
                        );
                        $("#" + btn_id).html(original_html);
                        reject(response);
                    }
                },
            });
        } else {
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: "json",
                success: function (response) {
                    if (response.type == "Success") {
                        notify_toast(
                            "la la-check-circle",
                            response.type,
                            "success-type",
                            response.text
                        );
                    } else {
                        notify_toast(
                            "la la-warning",
                            response.type,
                            "error-type",
                            response.text
                        );
                    }
                    $("#" + btn_id).removeClass(
                        "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
                    );
                    $("#" + btn_id).html(original_html);
                    resolve(response);
                },
                error: function (response) {
                    if (response.status == "401") {
                        notify_toast(
                            "la la-warning",
                            "Warning",
                            "warning-type",
                            "session expired. please login to continue"
                        );

                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    } else {
                        notify_toast(
                            "la la-warning",
                            "Warning",
                            "warning-type",
                            "unknown error occurred. please contact management"
                        );
                        $("#" + btn_id).removeClass(
                            "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
                        );
                        $("#" + btn_id).html(original_html);
                        reject(response);
                    }
                },
            });
        }
    });
}

function change_status(changeTo, id, url) {
    data = {
        id: id,
        changeTo: changeTo,
    };

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: "json",
        success: function (response) {
            if (response.type == "Success") {
                notify_toast(
                    "la la-check-circle",
                    response.type,
                    "success-type",
                    response.text
                );
                pulseDtTable.ajax.reload();
            } else {
                notify_toast(
                    "la la-warning",
                    response.type,
                    "error-type",
                    response.text
                );
            }
        },
        error: function (response) {
            if (response.status == "401") {
                notify_toast(
                    "la la-warning",
                    "Warning",
                    "warning-type",
                    "session expired. please login to continue"
                );

                setTimeout(() => {
                    location.reload();
                }, 1000);
            } else {
                notify_toast(
                    "la la-warning",
                    "Warning",
                    "warning-type",
                    "unknown error occurred. please contact management"
                );
            }
        },
    });
}

function removeDtItem(url) {
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        success: function (response) {
            if (response.type == "Success") {
                notify_toast(
                    "la la-check-circle",
                    response.type,
                    "success-type",
                    response.text
                );
                pulseDtTable.ajax.reload();
            } else {
                notify_toast(
                    "la la-warning",
                    response.type,
                    "error-type",
                    response.text
                );
            }
        },
        error: function (response) {
            if (response.status == "401") {
                notify_toast(
                    "la la-warning",
                    "Warning",
                    "warning-type",
                    "session expired. please login to continue"
                );

                setTimeout(() => {
                    location.reload();
                }, 1000);
            } else {
                notify_toast(
                    "la la-warning",
                    "Warning",
                    "warning-type",
                    "unknown error occurred. please contact management"
                );
            }
        },
    });
}

function removeDtItem(id, url) {
    removeItem(id, url)
        .then((data) => {
            if (data.type == "Success") {
                pulseDtTable.ajax.reload();
            }
        })
        .catch((error) => {});
}
