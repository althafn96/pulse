var table = $("#kt_table_2")
    .DataTable({
        ajax: "surveys",
        responsive: true,
        searchDelay: 500,
        processing: true,
        serverSide: true,
        ordering: false,
        language: {
            processing: `
                <div class="indicator"> 
                    <svg width="16px" height="12px">
                        <polyline id="back" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
                        <polyline id="front" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
                    </svg>
                </div>
                `,
        },
        pageLength: 9,
        lengthMenu: [9, 30, 60, 90, 300],
        columns: [
            {
                data: "top",
                className: "kt-widget__head d-flex",
            },
            {
                data: "middle",
                className: "kt-widget__body",
            },
            {
                data: "bottom",
                class: "kt-widget__footer",
            },
        ],
        drawCallback: function (settings) {
            var api = this.api();
            var $table = $(api.table().node());

            console.log(settings.aoData.length);
            if (settings.aoData.length == 0) {
                $("#kt_table_2").removeClass("cards");
                $("tbody tr", $table).each(function () {
                    $(this).css(
                        "box-shadow",
                        "0px 0px 13px 0px rgba(82, 63, 105, 0)"
                    );
                    $(this).css("border", "0");
                });
            }

            if ($table.hasClass("cards")) {
                // $(dt.table().node()).toggleClass('cards');
                // $('.fa', node).toggleClass(['fa-table', 'fa-id-badge']);

                // Create an array of labels containing all table headers
                var labels = [];
                $("thead th", $table).each(function () {
                    labels.push($(this).text());
                });

                $("tbody").addClass("row");

                // Add data-label attribute to each cell
                $("tbody tr", $table).each(function () {
                    $(this)
                        .find("td")
                        .each(function (column) {
                            // $(this).attr('data-label', labels[column]);
                        });
                    $(this).addClass("kt-widget kt-widget--project-1 p-0");
                    $(this).removeAttr("role");
                });

                var max = 400;
                $("tbody tr", $table)
                    .each(function () {
                        max = Math.max($(this).height(), max);
                    })
                    .height(max);
            } else {
                // Remove data-label attribute from each cell
                $("tbody td", $table).each(function () {
                    $(this).removeAttr("data-label");
                });

                $("tbody tr", $table).each(function () {
                    $(this).height("auto");
                });
            }
        },
    })
    .on("draw", function (e, dt, type, indexes) {
        $("#kt_table_2").addClass("cards");
        $("#kt_table_2").removeClass("table-striped");
    });

jQuery(document).ready(function () {
    $("#kt_table_2").addClass("cards");

    $("#survey_category").select2({
        ajax: {
            url: "survey-categories/load-survey-categories-to-select",
            dataType: "json",
            data: function (params) {
                var query = {
                    search: params.term,
                    type: "public",
                };

                return query;
            },

            processResults: function (data) {
                // Transforms the top-level key of the response object from 'items' to 'results'
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.title,
                            id: item.id,
                        };
                    }),
                };
            },
        },
        width: "100%",
    });

    $("#survey_category").change(function () {
        if ($("#survey_category").val() == "-1") {
            alert("dd");
        }
    });
});

$("#create_survey").click(function () {
    data = {
        title: $("#survey_title").val(),
        survey_category: $("#survey_category").val(),
    };

    url = "surveys/create-survey";

    $(this).addClass(
        "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
    );
    $(this).html("loading...");

    addItem(url, data)
        .then((data) => {
            if (data.type == "Info") {
                notify_toast(
                    "la la-flask",
                    data.type,
                    "info-type",
                    data.text,
                    "bottom",
                    "center"
                );

                setTimeout(() => {
                    window.location.href =
                        $(this).data("url") + "/" + data.survey_id;
                }, 1000);
            } else {
                $(this).removeClass(
                    "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
                );
                $(this).html("Submit");
            }
        })
        .catch((error) => {
            notify_toast(
                "la la-warning",
                "Warning",
                "warning-type",
                "unknown error occurred. please contact management"
            );

            $(this).removeClass(
                "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
            );
            $(this).html("Submit");
        });
});
