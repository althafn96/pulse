$(document).ready(function () {
    $(".select2").select2();
    $(".summernote").summernote({
        // callbacks: {
        //     onChange: function (e) {
        //         saveTextAreaValue($(this).summernote('code'), $(this).attr('id'), $(this).data('sid'));
        //     },
        // },
        height: 150,
    });
    avatar = new KTAvatar("kt_user_add_avatar");

    $(".survey_date_time").datetimepicker({
        todayHighlight: true,
        autoclose: true,
        format: "yyyy-mm-dd hh:ii",
    });

    $(".load-survey-attribute-fields").selectpicker();
});
// $('#survey_bg_image').change(function () {
//     var dataimg = new FormData();
//     s_id = $(this).data('sid');
//     dataimg.append('background_image', $("#survey_bg_image")[0].files[0]);

//     updateSurveyValue(dataimg, s_id, 'image');
// });

// function saveTextAreaValue(val, field) {
//     s_id = $(this).data('sid');
//     data = {
//         field: field,
//         value: val
//     }

//     updateSurveyValue(data, s_id, 'text');
// }

function updateSurveyValue(data, s_id, type) {
    url = "../update/" + s_id;

    if (type == "image") {
        $.ajax({
            type: "POST",
            processData: false,
            contentType: false,
            url: url,
            dataType: "json",
            data: data,
            success: function (response) {},
            error: function (response) {},
        });
    } else {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: data,
            success: function (response) {},
            error: function (response) {},
        });
    }
}

// Instantiate the Bloodhound suggestion engine
var questions = new Bloodhound({
    datumTokenizer: function (datum) {
        return Bloodhound.tokenizers.whitespace(datum.value);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        wildcard: "%QUERY",
        url: "../../surveys/fetch-questions?question=%QUERY",
        transform: function (response) {
            // Map the remote source JSON array to a JavaScript object array
            return $.map(response, function (question) {
                return {
                    value: question.question,
                };
            });
        },
    },
});
// Instantiate the Typeahead UI
$("#question").typeahead(null, {
    display: "value",
    source: questions,
});

var answers = new Bloodhound({
    datumTokenizer: function (datum) {
        return Bloodhound.tokenizers.whitespace(datum.value);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        wildcard: "%QUERY",
        url: "../../surveys/fetch-answers?answer=%QUERY",
        transform: function (response) {
            // Map the remote source JSON array to a JavaScript object array
            return $.map(response, function (answer) {
                return {
                    value: answer.answer,
                };
            });
        },
    },
});

Sortable.create(sc_questions_list, {
    scroll: true,
    store: {
        set: function (sortable) {
            // let order = sortable.toArray();
            // console.log(order);
            let order = {};
            $(".kt-widget2__item").each(function () {
                order[$(this).data("id")] = $(this).index();
            });
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                },
            });
            $.ajax({
                url: "../change-question-order",
                type: "POST",
                data: order,
                success: function (response) {
                    console.log(response);
                },
                error: function (response) {
                    if (response.status == "401") {
                        notify_toast(
                            "la la-warning",
                            "Warning",
                            "warning-type",
                            "session expired. please login to continue"
                        );

                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    }
                },
            });
        },
    },
});

Sortable.create(sc_question_answers_list, {
    scroll: true,
    store: {
        set: function (sortable) {
            // let order = sortable.toArray();
            // console.log(order);
            let order = {};
            $(".kt-widget2__item").each(function () {
                order[$(this).data("id")] = $(this).index();
            });
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                },
            });
            $.ajax({
                url: "../change-answers-order",
                type: "POST",
                data: order,
                success: function (response) {
                    console.log(response);
                },
                error: function (response) {
                    if (response.status == "401") {
                        notify_toast(
                            "la la-warning",
                            "Warning",
                            "warning-type",
                            "session expired. please login to continue"
                        );

                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    }
                },
            });
        },
    },
});

// $('.answer_choice').typeahead(null, {
//     display: 'value',
//     source: answers
// });

$(".kt_repeater_1").repeater({
    initEmpty: false,

    defaultValues: {
        "text-input": "foo",
    },

    show: function () {
        $(this).slideDown();
        // $('.answer_choice').typeahead(null, {
        //     display: 'value',
        //     source: answers
        // });
    },

    hide: function (deleteElement) {
        $(this).slideUp(deleteElement);
    },
});

$("#store-item-btn").click(function () {
    btn_id = $(this).attr("id");
    url = $(this).data("url");

    fdata = new FormData($("#survey_add_form")[0]);
    fdata.append(
        "survey_description",
        $("#survey_description").summernote("code")
    );
    fdata.append(
        "survey_intro_note",
        $("#survey_intro_note").summernote("code")
    );
    fdata.append(
        "survey_thankyou_note",
        $("#survey_thankyou_note").summernote("code")
    );

    if ($("#survey_is_scheduled").is(":checked")) {
        fdata.append("survey_is_scheduled", "1");
    } else {
        fdata.append("survey_is_scheduled", "0");
    }

    updateItem(btn_id, url, fdata, "formdata")
        .then((data) => {
            if (data.type == "Success") {
                $("#" + btn_id)
                    .addClass(
                        "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
                    )
                    .html("loading...");

                setTimeout(() => {
                    window.location.href = $(".back-btn").attr("href");
                }, 1000);
            }
        })
        .catch((error) => {});
});

$("#attr_field").change(function () {
    attr_code = $("#attr_field").val();

    if (attr_code == "002" || attr_code == "003" || attr_code == "004") {
        $(".choices_section").removeAttr("style");
    } else {
        $('input[name*="attr_choices"]').each(function (e) {
            $(this).val("");
        });

        $(".choices_section").css("display", "none");
    }
});

// $('.survey_date_time').change(function () {
//     s_id = $(this).data('sid');
//     field = $(this).attr('id');
//     val = $(this).val();

//     data = {
//         field: field,
//         value: val
//     }

//     updateSurveyValue(data, s_id, 'date_time');
// });

$("#is_scheduled").click(function () {
    if ($(this).is(":checked")) {
        $(".date_time").css("display", "");
        val = "1";
    } else {
        $(".date_time").css("display", "none");
        val = "0";
    }
});

$("#add_question_to_survey").click(function () {
    choices = [];
    attr_code = $("#attr_field").val();
    btn_id = $(this).attr("id");
    $("#" + btn_id).addClass(
        "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
    );
    $("#" + btn_id).html("Loading");

    comment_field = "0";
    if (attr_code == "002" || attr_code == "003" || attr_code == "004") {
        $('input[name*="attr_choices"]').each(function (e) {
            if ($(this).val() !== "") {
                choices.push($(this).val());
            }
        });

        if ($("#comment_field").is(":checked")) {
            comment_field = "1";
        } else {
            comment_field = "0";
        }
    }

    if ($("#question_required").is(":checked")) {
        is_required = "1";
    } else {
        is_required = "0";
    }

    data = {
        question: $("#question").val(),
        attr: $("#attr_field").val(),
        choices: choices,
        is_required: is_required,
        comment_field: comment_field,
    };

    addItem($(this).data("url"), data)
        .then((data) => {
            if (data.type == "Success") {
                notify_toast(
                    "la la-check-circle",
                    data.type,
                    "success-type",
                    data.text
                );

                $("#question").val("");
                $('input[name*="attr_choices"]').each(function (e) {
                    $(this).val("");
                });

                is_required = data.question.is_required == "1" ? "*" : "";
                comment_box =
                    data.question.comment_box == "1"
                        ? ' <textarea class="form-control mt-1" disabled name="" id="" rows="3">Additional Comments</textarea>'
                        : "";
                choices = data.choices;

                question_item =
                    `
                <div data-id="` +
                    data.question.id +
                    `" class="kt-widget2__item kt-widget2__item--brand question-item-` +
                    data.question.id +
                    `">
                    <div class="kt-widget2__checkbox">
                        
                    </div>
                    <div style="width: 100%" class="kt-widget2__info">
                        <a href="#" onclick="return false;" class="kt-widget2__title">
                        ` +
                    data.question.question +
                    ` ` +
                    is_required +
                    `
                        </a>
                `;
                if (data.question.field_code == "001") {
                    question_item += `
                        <input disabled class="form-control" type="text">
                    `;
                } else if (data.question.field_code == "002") {
                    question_item += `<div class="kt-radio-list">`;
                    choices.forEach(function (item, index) {
                        question_item +=
                            `<label class="kt-radio">
                                            <input disabled type="radio" name="radio1">` +
                            item +
                            `
                                            <span></span>
                                        </label>`;
                    });

                    question_item += `</div>`;
                } else if (data.question.field_code == "003") {
                    question_item += `<div class="kt-checkbox-list">`;
                    choices.forEach(function (item, index) {
                        question_item +=
                            `<label class="kt-checkbox">
                                            <input disabled type="checkbox" name="checkbox1">` +
                            item +
                            `
                                            <span></span>
                                        </label>`;
                    });

                    question_item += `</div>`;
                } else if (data.question.field_code == "004") {
                    question_item += `<select class="select2">
                                        <option value="">--SELECT--</option>`;
                    choices.forEach(function (item, index) {
                        question_item +=
                            `<option disabled >` + item + `</option>`;
                    });

                    question_item += `</select>`;
                }
                question_item += comment_box + `</div>`;

                question_item +=
                    `
                                <div class="kt-widget2__actions">
                                    <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                        <i class="flaticon-more-1"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a data-id="` +
                    data.question.id +
                    `" data-url="edit-survey-question" class="kt-nav__link edit-item">
                                                    <i class="kt-nav__link-icon flaticon2-gear"></i>
                                                    <span class="kt-nav__link-text">Edit</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a data-id="` +
                    data.question.id +
                    `" data-url="change-answers-order" class="kt-nav__link edit-answers-order">
                                                    <i class="kt-nav__link-icon flaticon2-sort"></i>
                                                    <span class="kt-nav__link-text">Sort Answers</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a data-id="` +
                    data.question.id +
                    `" data-url="remove-survey-question" class="kt-nav__link remove-item">
                                                    <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                    <span class="kt-nav__link-text">Remove</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div> 
                `;
                $("#sc_questions_list").append(question_item);
                $(".select2").select2();
            } else {
                notify_toast(
                    "la la-warning",
                    data.type,
                    "error-type",
                    data.text
                );
            }

            $("#" + btn_id).removeClass(
                "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
            );
            $("#" + btn_id).html("Add Attribute");
        })
        .catch((error) => {
            notify_toast(
                "la la-warning",
                "Warning",
                "warning-type",
                "unknown error occurred. please contact management"
            );
            $("#" + btn_id).removeClass(
                "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
            );
            $("#" + btn_id).html("Add Attribute");
        });
});

$("#questions_list").on("click", ".edit-item", function () {
    id = $(this).data("id");
    url = "../" + $(this).data("url");

    editItem(id, url)
        .then((data) => {
            if (data.type == "Success") {
                question_answers = data.answers;
                attr_list_res = data.attribute_fields;
                attr_fields_list = "";

                // console.log(data.question);
                // console.log(data.answers);
                // console.log(data.attribute_fields);

                attr_list_res.forEach(function (item, index) {
                    if (item.code == data.question.temp_field_code) {
                        attr_fields_list +=
                            `<option id="id_selected_` +
                            item.code +
                            `" data-value="` +
                            item.title +
                            `" selected value="` +
                            item.code +
                            `" data-icon="` +
                            item.icon +
                            `">` +
                            item.title +
                            `</option>`;
                    } else {
                        attr_fields_list +=
                            `<option id="id_selected_` +
                            item.code +
                            `" data-value="` +
                            item.title +
                            `" value="` +
                            item.code +
                            `" data-icon="` +
                            item.icon +
                            `">` +
                            item.title +
                            `</option>`;
                    }
                });

                repeater_fields = "";

                if (
                    data.question.temp_field_code == "002" ||
                    data.question.temp_field_code == "003" ||
                    data.question.temp_field_code == "004"
                ) {
                    repeater_fields +=
                        '<label class="col-xl-2 col-lg-2 col-form-label mt-3 choices_section choices_section_' +
                        data.question.id +
                        '">choices*</label>';

                    question_answers.forEach(function (item, index) {
                        if (index == 0) {
                            repeater_fields +=
                                `
                                <div class="col-lg-10 col-xl-10 mt-3 choices_section choices_section_` +
                                data.question.id +
                                `">
                                    <div class="kt_repeater_2" id="kt_repeater_2">
                                        <div class="kt_repeater_2" id="kt_repeater_2">

                                            <div data-repeater-list="">
                                                <div data-repeater-item class="mb-3 row align-items-center">
                                                    <div class="col-lg-5">
                                                        <input value="` +
                                item.answer +
                                `" id="attr_choices" class="form-control sc_field attr_choices_` +
                                id +
                                `" type="text" name="attr_choices">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                            <i class="la la-trash-o"></i>
                                                            Delete
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `;
                        } else {
                            repeater_fields +=
                                `
                                <div class="col-lg-10 col-xl-10 offset-lg-2 offset-xl-2 mt-3 choices_section choices_section_` +
                                data.question.id +
                                `">
                                    <div class="kt_repeater_2" id="kt_repeater_2">
                                        <div class="kt_repeater_2" id="kt_repeater_2">

                                            <div data-repeater-list="">
                                                <div data-repeater-item class="mb-3 row align-items-center">
                                                    <div class="col-lg-5">
                                                        <input value="` +
                                item.answer +
                                `" id="attr_choices" class="form-control sc_field attr_choices_` +
                                id +
                                `" type="text" name="attr_choices">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                            <i class="la la-trash-o"></i>
                                                            Delete
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `;
                        }
                    });

                    repeater_fields +=
                        `
                                            <div class="col-lg-10 col-xl-10 mt-3 offset-lg-2 offset-xl-2 choices_section choices_section_` +
                        data.question.id +
                        `">
                                                <div class="kt_repeater_2" id="kt_repeater_2">
                                                    <div class="kt_repeater_2" id="kt_repeater_2">

                                                        <div data-repeater-list="">
                                                            <div data-repeater-item class="mb-3 row align-items-center">
                                                                <div class="col-lg-5">
                                                                    <input id="attr_choices" class="form-control sc_field attr_choices_` +
                        id +
                        `" type="text" placeholder="Enter Option" name="attr_choices">
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                                        <i class="la la-trash-o"></i>
                                                                        Delete
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-last row">
                                                        <div class="col-lg-4">
                                                            <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                                                <i class="la la-plus"></i> Add
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        `;

                    if (data.question.temp_comment_box == "1") {
                        comment_box =
                            `
                        <div class="offset-md-8 offset-lg-8 col-lg-4 col-xl-4 mt-3 choices_section choices_section_` +
                            data.question.id +
                            `">
                            <div class="kt-checkbox-list">
                                <label class="kt-checkbox">
                                    <input checked id="comment_field_` +
                            data.question.id +
                            `" type="checkbox"> Add a Comment Field 
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        `;
                    } else {
                        comment_box =
                            `
                        <div class="offset-md-8 offset-lg-8 col-lg-4 col-xl-4 mt-3 choices_section choices_section_` +
                            data.question.id +
                            `">
                            <div class="kt-checkbox-list">
                                <label class="kt-checkbox">
                                    <input id="comment_field_` +
                            data.question.id +
                            `" type="checkbox"> Add a Comment Field 
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        `;
                    }
                } else {
                    repeater_fields +=
                        `
                                            <div style="display: none" class="col-lg-10 col-xl-10 mt-3 offset-lg-2 offset-xl-2 choices_section choices_section_` +
                        data.question.id +
                        `">
                                                <div class="kt_repeater_2" id="kt_repeater_2">
                                                    <div class="kt_repeater_2" id="kt_repeater_2">

                                                        <div data-repeater-list="">
                                                            <div data-repeater-item class="mb-3 row align-items-center">
                                                                <div class="col-lg-5">
                                                                    <input id="attr_choices" class="form-control sc_field attr_choices_` +
                        id +
                        `" type="text" placeholder="Enter Option" name="attr_choices">
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                                        <i class="la la-trash-o"></i>
                                                                        Delete
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-last row">
                                                        <div class="col-lg-4">
                                                            <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                                                <i class="la la-plus"></i> Add
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        `;

                    comment_box =
                        `
                                    <div style="display: none" class="offset-md-8 offset-lg-8 col-lg-4 col-xl-4 mt-3 choices_section choices_section_` +
                        data.question.id +
                        `">
                                        <div class="kt-checkbox-list">
                                            <label class="kt-checkbox">
                                                <input id="comment_field_` +
                        data.question.id +
                        `" type="checkbox"> Add a Comment Field 
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                        `;
                }

                if (data.question.temp_is_required == "1") {
                    is_required =
                        `
                    <div class="offset-md-8 offset-lg-8 col-lg-4 col-xl-4 mt-3">
                        <div class="kt-checkbox-list">
                            <label class="kt-checkbox">
                                <input checked id="question_required_` +
                        data.question.id +
                        `" type="checkbox"> Required
                                <span></span>
                            </label>
                        </div>
                    </div>
                    `;
                } else {
                    is_required =
                        `
                    <div class="offset-md-8 offset-lg-8 col-lg-4 col-xl-4 mt-3">
                        <div class="kt-checkbox-list">
                            <label class="kt-checkbox">
                                <input id="question_required_` +
                        data.question.id +
                        `" type="checkbox"> Required
                                <span></span>
                            </label>
                        </div>
                    </div>
                    `;
                }

                question_edit_form =
                    `
                    <div class="form-group row">
                        <label class="col-xl-2 col-lg-2 col-form-label">Question *</label>
                        <div class="col-lg-10 col-xl-10">
                            <input id="question-` +
                    data.question.id +
                    `" class="form-control sc_field" type="text" value="` +
                    data.question.temp_question +
                    `" name="question">
                        </div >
                        <label class="col-xl-2 col-lg-2 col-form-label float-right mt-3" for="attr_field_` +
                    data.question.id +
                    `">Answer Type</label>
                        <div class="col-lg-10 col-xl-10 mt-3">
                            <select onChange="changeAttrFieldVal(this.value, '` +
                    data.question.id +
                    `')" data-live-search="true" class="form-control load-survey-attribute-fields" id="attr_field_` +
                    data.question.id +
                    `">
                            ` +
                    attr_fields_list +
                    `

                                </select>
                        </div>
                        ` +
                    is_required +
                    `
                        ` +
                    repeater_fields +
                    `
                        ` +
                    comment_box +
                    `

                        <div class="col-lg-12 mt-3">
                            <div class="col-lg-2 float-right pr-0">
                                <button id="update-` +
                    data.question.id +
                    `-btn" data-id="` +
                    data.question.id +
                    `" data-url="update-question" class="form-control btn btn-brand btn-md update_attribute_to_sc" type="button">Update</button>
                            </div>
                            <div class="col-lg-2 float-right pr-0">
                                <input type="hidden" >
                                <button id="cancel-update-` +
                    data.question.id +
                    `-btn" data-id="` +
                    data.question.id +
                    `"class="form-control btn btn-secondary btn-md cancel_update_attribute_to_sc" type="button">Cancel</button>
                            </div>
                        </div>
                    </div >
                    `;

                $(".question-item-" + id).removeClass(
                    "kt-widget2__item kt-widget2__item--brand"
                );
                $(".question-item-" + id).css(
                    "borderBottom",
                    "0.5px solid #f2f3f7"
                );
                $(".question-item-" + id).css("marginBottom", "1.4rem");
                $(".question-item-" + id).html(question_edit_form);
                $(".load-survey-attribute-fields").selectpicker();

                $(".kt_repeater_2").repeater({
                    initEmpty: false,

                    defaultValues: {
                        "text-input": "foo",
                    },

                    show: function () {
                        $(this).slideDown();
                    },

                    hide: function (deleteElement) {
                        $(this).slideUp(deleteElement);
                    },
                });
            }
        })
        .catch((error) => {});
});

function changeAttrFieldVal(attr_code, id) {
    if (attr_code == "002" || attr_code == "003" || attr_code == "004") {
        $(".choices_section_" + id).removeAttr("style");
    } else {
        $(".choices_section_" + id).css("display", "none");
    }
}

$("#sc_questions_list").on("click", ".update_attribute_to_sc", function () {
    btn_id = $(this).attr("id");

    question_id = $(this).data("id");
    url = "../update-question";

    choices = [];
    attr_code = $("#attr_field_" + id).val();

    comment_field = "0";
    if (attr_code == "002" || attr_code == "003" || attr_code == "004") {
        $('input[class*="attr_choices_' + id + '"]').each(function (e) {
            if ($(this).val() !== "") {
                choices.push($(this).val());
            }
        });

        if ($("#comment_field_" + id).is(":checked")) {
            comment_field = "1";
        } else {
            comment_field = "0";
        }
    }

    if ($("#question_required_" + id).is(":checked")) {
        is_required = "1";
    } else {
        is_required = "0";
    }

    data = {
        id: id,
        question: $("#question-" + id).val(),
        attr: $("#attr_field_" + id).val(),
        is_required: is_required,
        comment_field: comment_field,
        choices: choices,
    };

    updateItem(btn_id, url, data)
        .then((data) => {
            if (data.type == "Success") {
                id = data.question.id;
                is_required = data.question.temp_is_required == "1" ? "*" : "";
                comment_box =
                    data.question.temp_comment_box == "1"
                        ? ' <textarea class="form-control mt-1" disabled name="" id="" rows="3">Additional Comments</textarea>'
                        : "";
                choices = data.choices;

                question_item =
                    `
                    <div class="kt-widget2__checkbox">
                        
                    </div>
                    <div style="width: 100%" class="kt-widget2__info">
                        <a href="#" onclick="return false;" class="kt-widget2__title">
                        ` +
                    data.question.temp_question +
                    ` ` +
                    is_required +
                    `
                        </a>
                `;
                if (data.question.temp_field_code == "001") {
                    question_item += `
                        <input disabled class="form-control" type="text">
                    `;
                } else if (data.question.temp_field_code == "002") {
                    question_item += `<div class="kt-radio-list">`;
                    choices.forEach(function (item, index) {
                        question_item +=
                            `<label class="kt-radio">
                                            <input disabled type="radio" name="radio1">` +
                            item +
                            `
                                            <span></span>
                                        </label>`;
                    });

                    question_item += `</div>`;
                } else if (data.question.temp_field_code == "003") {
                    question_item += `<div class="kt-checkbox-list">`;
                    choices.forEach(function (item, index) {
                        question_item +=
                            `<label class="kt-checkbox">
                                            <input disabled type="checkbox" name="checkbox1">` +
                            item +
                            `
                                            <span></span>
                                        </label>`;
                    });

                    question_item += `</div>`;
                } else if (data.question.temp_field_code == "004") {
                    question_item += `<select class="select2">
                                        <option value="">--SELECT--</option>`;
                    choices.forEach(function (item, index) {
                        question_item +=
                            `<option disabled >` + item + `</option>`;
                    });

                    question_item += `</select>`;
                }
                question_item += comment_box + `</div>`;

                question_item +=
                    `
                                <div class="kt-widget2__actions">
                                    <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                        <i class="flaticon-more-1"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a data-id="` +
                    data.question.id +
                    `" data-url="edit-survey-question" class="kt-nav__link edit-item">
                                                    <i class="kt-nav__link-icon flaticon2-gear"></i>
                                                    <span class="kt-nav__link-text">Edit</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a data-id="` +
                    data.question.id +
                    `" data-url="change-answers-order" class="kt-nav__link edit-answers-order">
                                                    <i class="kt-nav__link-icon flaticon2-sort"></i>
                                                    <span class="kt-nav__link-text">Sort Answers</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a data-id="` +
                    data.question.id +
                    `" data-url="remove-survey-question" class="kt-nav__link remove-item">
                                                    <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                    <span class="kt-nav__link-text">Remove</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                `;

                $(".question-item-" + id).addClass(
                    "kt-widget2__item kt-widget2__item--brand"
                );
                $(".question-item-" + id).removeAttr("style");

                $(".question-item-" + id).html(question_item);
                $(".select2").select2();
            }
        })
        .catch((error) => {});
});

$("#sc_questions_list").on(
    "click",
    ".cancel_update_attribute_to_sc",
    function () {
        id = $(this).data("id");
        url = "../get-question";
        btn_id = $(this).attr("id");

        get_question(id, url, btn_id)
            .then((data) => {
                if (data.type == "Success") {
                    id = data.question.id;
                    is_required =
                        data.question.temp_is_required == "1" ? "*" : "";
                    comment_box =
                        data.question.temp_comment_box == "1"
                            ? ' <textarea class="form-control mt-1" disabled name="" id="" rows="3">Additional Comments</textarea>'
                            : "";
                    choices = data.choices;

                    question_item =
                        `
                    <div class="kt-widget2__checkbox">
                        
                    </div>
                    <div style="width: 100%" class="kt-widget2__info">
                        <a href="#" onclick="return false;" class="kt-widget2__title">
                        ` +
                        data.question.temp_question +
                        ` ` +
                        is_required +
                        `
                        </a>
                `;
                    if (data.question.temp_field_code == "001") {
                        question_item += `
                        <input disabled class="form-control" type="text">
                    `;
                    } else if (data.question.temp_field_code == "002") {
                        question_item += `<div class="kt-radio-list">`;
                        choices.forEach(function (item, index) {
                            question_item +=
                                `<label class="kt-radio">
                                            <input disabled type="radio" name="radio1">` +
                                item +
                                `
                                            <span></span>
                                        </label>`;
                        });

                        question_item += `</div>`;
                    } else if (data.question.temp_field_code == "003") {
                        question_item += `<div class="kt-checkbox-list">`;
                        choices.forEach(function (item, index) {
                            question_item +=
                                `<label class="kt-checkbox">
                                            <input disabled type="checkbox" name="checkbox1">` +
                                item +
                                `
                                            <span></span>
                                        </label>`;
                        });

                        question_item += `</div>`;
                    } else if (data.question.temp_field_code == "004") {
                        question_item += `<select class="select2">
                                        <option value="">--SELECT--</option>`;
                        choices.forEach(function (item, index) {
                            question_item +=
                                `<option disabled >` + item + `</option>`;
                        });

                        question_item += `</select>`;
                    }
                    question_item += comment_box + `</div>`;

                    question_item +=
                        `
                                <div class="kt-widget2__actions">
                                    <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                        <i class="flaticon-more-1"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a data-id="` +
                        data.question.id +
                        `" data-url="edit-survey-question" class="kt-nav__link edit-item">
                                                    <i class="kt-nav__link-icon flaticon2-gear"></i>
                                                    <span class="kt-nav__link-text">Edit</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a data-id="` +
                        data.question.id +
                        `" data-url="change-answers-order" class="kt-nav__link edit-answers-order">
                                                    <i class="kt-nav__link-icon flaticon2-sort"></i>
                                                    <span class="kt-nav__link-text">Sort Answers</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a data-id="` +
                        data.question.id +
                        `" data-url="remove-survey-question" class="kt-nav__link remove-item">
                                                    <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                    <span class="kt-nav__link-text">Remove</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                `;

                    $(".question-item-" + id).addClass(
                        "kt-widget2__item kt-widget2__item--brand"
                    );
                    $(".question-item-" + id).removeAttr("style");

                    $(".question-item-" + id).html(question_item);
                    $(".select2").select2();
                }
            })
            .catch((error) => {});
    }
);

function get_question(id, url, btn_id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "GET",
            url: url,
            data: "id=" + id,
            dataType: "json",
            success: function (response) {
                resolve(response);
            },
            error: function (response) {
                if (response.status == "401") {
                    notify_toast(
                        "la la-warning",
                        "Warning",
                        "warning-type",
                        "session expired. please login to continue"
                    );

                    setTimeout(() => {
                        location.reload();
                    }, 1000);
                } else {
                    notify_toast(
                        "la la-warning",
                        "Warning",
                        "warning-type",
                        "unknown error occurred. please contact management"
                    );
                    $("#" + btn_id).removeClass(
                        "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
                    );
                    $("#" + btn_id).html(original_html);
                    reject(data);
                }
            },
        });
    });
}

$("#sc_questions_list").on("click", ".edit-answers-order", function () {
    id = $(this).data("id");
    url = "../" + $(this).data("url") + "/" + id;

    $.ajax({
        type: "GET",
        url: url,
        dataType: "json",
        success: function (response) {
            var answers_list = "";

            response.forEach(function (item, index) {
                answers_list +=
                    `
                        <div data-id="` +
                    item.id +
                    `" class="kt-widget2__item kt-widget2__item--brand answer-` +
                    item.id +
                    `" >
                            <div class="kt-widget2__checkbox">
    
                            </div>
                            <div class="kt-widget2__info">
                                <a href="#" onclick="return false;" class="kt-widget2__title">
                                    ` +
                    item.answer +
                    `
                                </a>
                            </div>
                            <div class="kt-widget2__actions">
                            </div>
                        </div >
                        `;
            });

            $("#sc_question_answers_list").html(answers_list);
            $("#sort_answers_modal").attr("data-id", response[0].question_id);
            $("#sortAnswersModalLabel").html(response[0].question);
            $("#sort_answers_modal").modal({
                backdrop: "static",
                keyboard: false,
            });
        },
        error: function (response) {
            if (response.status == "401") {
                notify_toast(
                    "la la-warning",
                    "Warning",
                    "warning-type",
                    "session expired. please login to continue"
                );

                setTimeout(() => {
                    location.reload();
                }, 1000);
            } else {
                notify_toast(
                    "la la-warning",
                    "Warning",
                    "warning-type",
                    "unknown error occurred. please contact management"
                );
            }
        },
    });
});

$("#sc_questions_list").on("click", ".remove-item", function () {
    id = $(this).data("id");
    url = "../" + $(this).data("url");

    removeItem(id, url)
        .then((data) => {
            if (data.type == "Success") {
                $(".question-item-" + id).remove();
            }
        })
        .catch((error) => {});
});

$("#sort_answers_modal").on("hidden.bs.modal", function () {
    id = $(this).attr("data-id");
    url = "../get-question";
    btn_id = "";

    get_question(id, url, btn_id)
        .then((data) => {
            if (data.type == "Success") {
                id = data.question.id;
                is_required = data.question.temp_is_required == "1" ? "*" : "";
                comment_box =
                    data.question.temp_comment_box == "1"
                        ? ' <textarea class="form-control mt-1" disabled name="" id="" rows="3">Additional Comments</textarea>'
                        : "";
                choices = data.choices;

                question_item =
                    `
                    <div class="kt-widget2__checkbox">
                        
                    </div>
                    <div style="width: 100%" class="kt-widget2__info">
                        <a href="#" onclick="return false;" class="kt-widget2__title">
                        ` +
                    data.question.temp_question +
                    ` ` +
                    is_required +
                    `
                        </a>
                `;
                if (data.question.temp_field_code == "001") {
                    question_item += `
                        <input disabled class="form-control" type="text">
                    `;
                } else if (data.question.temp_field_code == "002") {
                    question_item += `<div class="kt-radio-list">`;
                    choices.forEach(function (item, index) {
                        question_item +=
                            `<label class="kt-radio">
                                            <input disabled type="radio" name="radio1">` +
                            item +
                            `
                                            <span></span>
                                        </label>`;
                    });

                    question_item += `</div>`;
                } else if (data.question.temp_field_code == "003") {
                    question_item += `<div class="kt-checkbox-list">`;
                    choices.forEach(function (item, index) {
                        question_item +=
                            `<label class="kt-checkbox">
                                            <input disabled type="checkbox" name="checkbox1">` +
                            item +
                            `
                                            <span></span>
                                        </label>`;
                    });

                    question_item += `</div>`;
                } else if (data.question.temp_field_code == "004") {
                    question_item += `<select class="select2">
                                        <option value="">--SELECT--</option>`;
                    choices.forEach(function (item, index) {
                        question_item +=
                            `<option disabled >` + item + `</option>`;
                    });

                    question_item += `</select>`;
                }
                question_item += comment_box + `</div>`;

                question_item +=
                    `
                                <div class="kt-widget2__actions">
                                    <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                        <i class="flaticon-more-1"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a data-id="` +
                    data.question.id +
                    `" data-url="edit-survey-question" class="kt-nav__link edit-item">
                                                    <i class="kt-nav__link-icon flaticon2-gear"></i>
                                                    <span class="kt-nav__link-text">Edit</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a data-id="` +
                    data.question.id +
                    `" data-url="change-answers-order" class="kt-nav__link edit-answers-order">
                                                    <i class="kt-nav__link-icon flaticon2-sort"></i>
                                                    <span class="kt-nav__link-text">Sort Answers</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a data-id="` +
                    data.question.id +
                    `" data-url="remove-survey-question" class="kt-nav__link remove-item">
                                                    <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                    <span class="kt-nav__link-text">Remove</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                `;

                $(".question-item-" + id).html(question_item);
                $(".select2").select2();
            }
        })
        .catch((error) => {});
});
