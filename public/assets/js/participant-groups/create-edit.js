jQuery(document).ready(function () {
    $(".load-participant-group-attribute-fields").selectpicker();

    if ($("#attr_list").attr("data-attrListCount") == "0") {
        $("#attr_list").css("display", "none");
    }
});

$(".kt_repeater_1").repeater({
    initEmpty: false,

    defaultValues: {
        "text-input": "foo",
    },

    show: function () {
        $(this).slideDown();
    },

    hide: function (deleteElement) {
        $(this).slideUp(deleteElement);
    },
});

$("#pg_title").keyup(function () {
    pg_title = $(this).val();
    url = $(this).data("url");

    $.ajax({
        type: "GET",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: url,
        data: "pg_title=" + pg_title,
        dataType: "json",
        success: function (response) {
            $("#pg_title").attr("data-temp-sc", response.id);
        },
        error: function (response) {
            if (response.status == "401") {
                notify_toast(
                    "la la-warning",
                    "Warning",
                    "warning-type",
                    "session expired. please login to continue"
                );

                setTimeout(() => {
                    location.reload();
                }, 1000);
            }
        },
    });
});

$("#attr_field").change(function () {
    attr_code = $("#attr_field").val();

    if (attr_code == "002" || attr_code == "003" || attr_code == "004") {
        $(".options_section").removeAttr("style");
    } else {
        $('input[name*="attr_options"]').each(function (e) {
            $(this).val("");
        });

        $(".options_section").css("display", "none");
    }
});

if (document.getElementById("pg_attr_list")) {
    Sortable.create(pg_attr_list, {
        scroll: true,
        store: {
            set: function (sortable) {
                // let order = sortable.toArray();
                // console.log(order);

                let order = {};
                $(".kt-widget2__item").each(function () {
                    order[$(this).data("id")] = $(this).index();
                });
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        ),
                    },
                });
                $.ajax({
                    url: "change-temp-attr-order",
                    type: "POST",
                    data: order,
                    success: function (response) {
                        console.log(response);
                    },
                    error: function (response) {
                        if (response.status == "401") {
                            notify_toast(
                                "la la-warning",
                                "Warning",
                                "warning-type",
                                "session expired. please login to continue"
                            );

                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        }
                    },
                });
            },
        },
    });
}

if (document.getElementById("edit_pg_attr_list")) {
    Sortable.create(edit_pg_attr_list, {
        scroll: true,
        store: {
            set: function (sortable) {
                // let order = sortable.toArray();
                // console.log(order);

                let order = {};
                $(".kt-widget2__item").each(function () {
                    order[$(this).data("id")] = $(this).index();
                });
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        ),
                    },
                });
                $.ajax({
                    url: "change-attr-order",
                    type: "POST",
                    data: order,
                    success: function (response) {
                        console.log(response);
                    },
                    error: function (response) {
                        if (response.status == "401") {
                            notify_toast(
                                "la la-warning",
                                "Warning",
                                "warning-type",
                                "session expired. please login to continue"
                            );

                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        }
                    },
                });
            },
        },
    });
}

$("#add_attribute_to_pg").click(function () {
    options = [];
    attr_code = $("#attr_field").val();
    btn_id = $(this).attr("id");
    $("#" + btn_id).addClass(
        "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
    );
    $("#" + btn_id).html("Loading");

    if (attr_code == "002" || attr_code == "003" || attr_code == "004") {
        $('input[name*="attr_options"]').each(function (e) {
            if ($(this).val() !== "") {
                options.push($(this).val());
            }
        });
    }
    participant_group_id = $(this).attr("data-participant-group");

    data = {
        title: $("#attr_title").val(),
        attr: $("#attr_field").val(),
        options: options,
        participant_group_id: participant_group_id,
    };

    addItem($(this).data("url"), data)
        .then((data) => {
            if (data.type == "Success") {
                notify_toast(
                    "la la-check-circle",
                    data.type,
                    "success-type",
                    data.text
                );

                $("#attr_title").val("");
                $('input[name*="attr_options"]').each(function (e) {
                    $(this).val("");
                });

                edit_url = $(".pg_attr_list").data("data-edit");
                remove_url = $(".pg_attr_list").data("data-remove");

                attr_list =
                    `
                    <div data-id="` +
                    data.data.id +
                    `" class="kt-widget2__item kt-widget2__item--brand attr-item-` +
                    data.data.id +
                    `" >
                        <div class="kt-widget2__checkbox">

                        </div>
                        <div class="kt-widget2__info">
                            <a href="#" onclick="return false;" class="kt-widget2__title">
                                ` +
                    data.data.title +
                    `
                            </a>
                            <a href="#" onclick="return false;" class="kt-widget2__username">
                                ` +
                    data.data.attr_title +
                    `
                            </a>
                            <a href="#" onclick="return false;" class="kt-widget2__username">
                                ` +
                    data.data.options +
                    `
                            </a>
                        </div>
                        <div class="kt-widget2__actions">
                            <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                <i class="flaticon-more-1"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a data-id="` +
                    data.data.id +
                    `" data-url="` +
                    edit_url +
                    `" class="kt-nav__link edit-item">
                                            <i class="kt-nav__link-icon flaticon2-gear"></i>
                                            <span class="kt-nav__link-text">Edit</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a data-id="` +
                    data.data.id +
                    `" data-url="` +
                    remove_url +
                    `" class="kt-nav__link remove-item">
                                            <i class="kt-nav__link-icon flaticon2-trash"></i>
                                            <span class="kt-nav__link-text">Remove</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div >
                    `;
                console.log(attr_list);

                $("#attr_list").removeAttr("style");

                if (document.getElementById("pg_attr_list")) {
                    $("#pg_attr_list").append(attr_list);
                } else if (document.getElementById("edit_pg_attr_list")) {
                    $("#edit_pg_attr_list").append(attr_list);
                }
            } else {
                notify_toast(
                    "la la-warning",
                    data.type,
                    "error-type",
                    data.text
                );
            }
            $("#" + btn_id).removeClass(
                "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
            );
            $("#" + btn_id).html("Add Attribute");
        })
        .catch((error) => {
            notify_toast(
                "la la-warning",
                "Warning",
                "warning-type",
                "unknown error occurred. please contact management"
            );
            $("#" + btn_id).removeClass(
                "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
            );
            $("#" + btn_id).html("Add Attribute");
        });
});

$(".pg_attr_list").on("click", ".remove-item", function () {
    id = $(this).data("id");
    url = $(".pg_attr_list").data("remove");

    removeItem(id, url)
        .then((data) => {
            if (data.type == "Success") {
                $(".attr-item-" + id).remove();
            }
        })
        .catch((error) => {});
});

$(".pg_attr_list").on("click", ".edit-item", function () {
    id = $(this).data("id");
    url = $(".pg_attr_list").data("edit");
    update_url = $(".pg_attr_list").data("update");

    editItem(id, url)
        .then((data) => {
            if (data.type == "Success") {
                attr_list_res = data.attribute_fields;
                attr_fields_list = "";

                attr_list_res.forEach(function (item, index) {
                    if (item.code == data.data.temp_field_code) {
                        attr_fields_list +=
                            `<option id="id_selected_` +
                            item.code +
                            `" data-value="` +
                            item.title +
                            `" selected value="` +
                            item.code +
                            `" data-icon="` +
                            item.icon +
                            `">` +
                            item.title +
                            `</option>`;
                    } else {
                        attr_fields_list +=
                            `<option id="id_selected_` +
                            item.code +
                            `" data-value="` +
                            item.title +
                            `" value="` +
                            item.code +
                            `" data-icon="` +
                            item.icon +
                            `">` +
                            item.title +
                            `</option>`;
                    }
                });

                repeater_fields = "";

                if (
                    data.data.temp_field_code == "002" ||
                    data.data.temp_field_code == "003" ||
                    data.data.temp_field_code == "004"
                ) {
                    repeater_fields +=
                        '<label class="col-xl-2 col-lg-2 col-form-label mt-3 options_section_for_edit options_for_edit_' +
                        id +
                        '">Options*</label>';

                    var opt_arr = JSON.parse(data.data.temp_options);
                    opt_arr.forEach(function (item, index) {
                        if (index == 0) {
                            repeater_fields +=
                                `
                                <div class="col-lg-10 col-xl-10 mt-3 options_section_for_edit options_for_edit_` +
                                id +
                                `">
                                    <div class="kt_repeater_2" id="kt_repeater_2">
                                        <div class="kt_repeater_2" id="kt_repeater_2">
                                            
                                            <div data-repeater-list="">
                                                <div data-repeater-item class="mb-3 row align-items-center">
                                                    <div class="col-lg-5">
                                                        <input value="` +
                                item +
                                `" id="attr_options" class="form-control sc_field attr_options_` +
                                id +
                                `" type="text" name="attr_options">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                            <i class="la la-trash-o"></i>
                                                            Delete
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `;
                        } else {
                            repeater_fields +=
                                `
                                <div class="col-lg-10 col-xl-10 offset-lg-2 offset-xl-2 mt-3 options_section_for_edit options_for_edit_` +
                                id +
                                `">
                                    <div class="kt_repeater_2" id="kt_repeater_2">
                                        <div class="kt_repeater_2" id="kt_repeater_2">
                                            
                                            <div data-repeater-list="">
                                                <div data-repeater-item class="mb-3 row align-items-center">
                                                    <div class="col-lg-5">
                                                        <input value="` +
                                item +
                                `" id="attr_options" class="form-control sc_field attr_options_` +
                                id +
                                `" type="text" name="attr_options">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                            <i class="la la-trash-o"></i>
                                                            Delete
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `;
                        }
                    });

                    repeater_fields +=
                        `
                            <div class="col-lg-10 col-xl-10 mt-3 offset-lg-2 offset-xl-2 options_section_for_edit options_for_edit_` +
                        id +
                        `">
                                <div class="kt_repeater_2" id="kt_repeater_2">
                                    <div class="kt_repeater_2" id="kt_repeater_2">
                                        
                                        <div data-repeater-list="">
                                            <div data-repeater-item class="mb-3 row align-items-center">
                                                <div class="col-lg-5">
                                                    <input id="attr_options" class="form-control sc_field attr_options_` +
                        id +
                        `" type="text" placeholder="Enter Option" name="attr_options">
                                                </div>
                                                <div class="col-md-5">
                                                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        Delete
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-last row">
                                        <div class="col-lg-4">
                                            <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                                <i class="la la-plus"></i> Add
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    `;
                } else {
                    repeater_fields +=
                        `
                        <div style="display:none" class="col-lg-10 col-xl-10 mt-3 offset-lg-2 offset-xl-2 options_section_for_edit options_for_edit_` +
                        id +
                        `" id="options_for_edit_` +
                        id +
                        `">
                            <div class="kt_repeater_2" id="kt_repeater_2">
                                <div class="kt_repeater_2" id="kt_repeater_2">
                                    
                                    <div data-repeater-list="">
                                        <div data-repeater-item class="mb-3 row align-items-center">
                                            <div class="col-lg-5">
                                                <input id="attr_options" class="form-control sc_field attr_options_` +
                        id +
                        `" type="text" placeholder="Enter Option" name="attr_options">
                                            </div>
                                            <div class="col-md-5">
                                                <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                    <i class="la la-trash-o"></i>
                                                    Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-group-last row">
                                    <div class="col-lg-4">
                                        <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                            <i class="la la-plus"></i> Add
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                `;
                }

                attr_edit_form =
                    `
                    <div class="form-group row">
                        <label class="col-xl-2 col-lg-2 col-form-label">Attribute Title *</label>
                        <div class="col-lg-4 col-xl-4">
                            <input id="attr_title_` +
                    id +
                    `" class="form-control sc_field" type="text" value="` +
                    data.data.temp_title +
                    `" name="attr_title">
                        </div >
                        <label class="col-xl-2 col-lg-2 col-form-label float-right" for="attr_field_` +
                    id +
                    `">Attribute Field</label>
                        <div class="col-lg-4 col-xl-4">
                            <select data-live-search="true" onChange="changeAttrFieldVal(this.value, '` +
                    id +
                    `')" class="form-control load-participant-group-attribute-fields attr-field-edit" id="attr_field_` +
                    id +
                    `">
                                ` +
                    attr_fields_list +
                    `
                                </select>
                        </div>
                        ` +
                    repeater_fields +
                    `
                        <div class="col-lg-12 mt-3">
                            <div class="col-lg-2 float-right pr-0">
                                <button id="update-` +
                    id +
                    `_btn" data-id="` +
                    data.data.id +
                    `" 
                    class="form-control btn btn-brand btn-md update_attribute_to_pg" type="button">Update</button>
                            </div>
                            <div class="col-lg-2 float-right pr-0">
                                <input type="hidden" id="attr_options_` +
                    data.data.id +
                    `" value='` +
                    data.data.temp_options +
                    `' >
                                <button id="cancel-update-` +
                    id +
                    `_btn" data-id="` +
                    data.data.id +
                    `"class="form-control btn btn-secondary btn-md cancel_update_attribute_to_pg" type="button">Cancel</button>
                            </div>
                        </div>
                    </div >
                    `;
                $(".attr-item-" + id).removeClass(
                    "kt-widget2__item kt-widget2__item--brand"
                );
                $(".attr-item-" + id).css(
                    "borderBottom",
                    "0.5px solid #f2f3f7"
                );
                $(".attr-item-" + id).css("marginBottom", "1.4rem");
                $(".attr-item-" + id).html(attr_edit_form);
                $(".load-participant-group-attribute-fields").selectpicker();

                $(".kt_repeater_2").repeater({
                    initEmpty: false,

                    defaultValues: {
                        "text-input": "foo",
                    },

                    show: function () {
                        $(this).slideDown();
                    },

                    hide: function (deleteElement) {
                        $(this).slideUp(deleteElement);
                    },
                });
            }
        })
        .catch((error) => {});
});

function changeAttrFieldVal(attr_code, id) {
    if (attr_code == "002" || attr_code == "003" || attr_code == "004") {
        $(".options_for_edit_" + id).removeAttr("style");
    } else {
        $(".options_for_edit_" + id).css("display", "none");
    }
}

function attrList(item, index) {
    edit_url = $(".pg_attr_list").data("data-edit");
    remove_url = $(".pg_attr_list").data("data-remove");

    attr_list +=
        `
                    <div data-id="` +
        item.id +
        `" class="kt-widget2__item kt-widget2__item--brand attr-item-` +
        item.id +
        `" >
                        <div class="kt-widget2__checkbox">

                        </div>
                        <div class="kt-widget2__info">
                            <a href="#" onclick="return false;" class="kt-widget2__title">
                                ` +
        item.title +
        `
                            </a>
                            <a href="#" onclick="return false;" class="kt-widget2__username">
                                ` +
        item.attr_title +
        `
                            </a>
                            <a href="#" onclick="return false;" class="kt-widget2__username">
                                ` +
        item.options +
        `
                            </a>
                        </div>
                        <div class="kt-widget2__actions">
                            <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                <i class="flaticon-more-1"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a data-id="` +
        item.id +
        `" data-url="` +
        edit_url +
        `" class="kt-nav__link edit-item">
                                            <i class="kt-nav__link-icon flaticon2-gear"></i>
                                            <span class="kt-nav__link-text">Edit</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a data-id="` +
        item.id +
        `" data-url="` +
        remove_url +
        `" class="kt-nav__link remove-item">
                                            <i class="kt-nav__link-icon flaticon2-trash"></i>
                                            <span class="kt-nav__link-text">Remove</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div >
                    `;
}

$("#store-item-btn").click(function () {
    $("#store-item-btn").addClass(
        "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
    );
    $("#store-item-btn").html("loading...");

    btn_id = $(this).attr("id");
    url = $(this).data("url");
    data = "";
    redirect_url = $(".back-btn").attr("href");

    addItem(url, data)
        .then((data) => {
            if (data.type == "Success") {
                notify_toast(
                    "la la-check-circle",
                    data.type,
                    "success-type",
                    data.text
                );

                if (redirect_url != null) {
                    setTimeout(() => {
                        window.location.href = redirect_url;
                    }, 2000);
                }
            } else {
                notify_toast(
                    "la la-warning",
                    data.type,
                    "error-type",
                    data.text
                );
                $("#store-item-btn").removeClass(
                    "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
                );
                $("#store-item-btn").html("Submit");
            }
        })
        .catch((error) => {
            notify_toast(
                "la la-warning",
                "Warning",
                "warning-type",
                "unknown error occurred. please contact management"
            );
            $("#store-item-btn").removeClass(
                "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
            );
            $("#store-item-btn").html("Submit");
        });
});

$(".pg_attr_list").on("click", ".update_attribute_to_pg", function () {
    id = $(this).data("id");
    url = $(".pg_attr_list").data("update");
    btn_id = $(this).attr("id");
    edit_url = $(".pg_attr_list").data("edit");

    options = [];

    $(".attr_options_" + id).each(function (e) {
        if ($(this).val() !== "") {
            options.push($(this).val());
        }
    });

    data = {
        id: id,
        title: $("#attr_title_" + id).val(),
        attr: $("#attr_field_" + id).val(),
        options: options,
    };

    updateItem(btn_id, url, data)
        .then((data) => {
            if (data.type == "Success") {
                attr_fields_list = "";

                attr_updated_field =
                    `
                        <div class="kt-widget2__checkbox">

                        </div>
                        <div class="kt-widget2__info">
                            <a href="#" onclick="return false;" class="kt-widget2__title">
                                ` +
                    data.data.temp_title +
                    `
                            </a>
                            <a href="#" onclick="return false;" class="kt-widget2__username">
                                ` +
                    data.data.attr_title +
                    `
                            </a>
                            <a href="#" onclick="return false;" class="kt-widget2__username">
                                ` +
                    data.data.temp_options +
                    `
                            </a>
                        </div>
                        <div class="kt-widget2__actions">
                            <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                <i class="flaticon-more-1"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a data-id="` +
                    data.data.id +
                    `" class="kt-nav__link edit-item">
                                            <i class="kt-nav__link-icon flaticon2-gear"></i>
                                            <span class="kt-nav__link-text">Edit</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a data-id="` +
                    data.data.id +
                    `" data-url="remove-temp-attr" class="kt-nav__link remove-item">
                                            <i class="kt-nav__link-icon flaticon2-trash"></i>
                                            <span class="kt-nav__link-text">Remove</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    `;
                $(".attr-item-" + id).addClass(
                    "kt-widget2__item kt-widget2__item--brand"
                );
                $(".attr-item-" + id).html(attr_updated_field);
                $(".load-participant-group-attribute-fields").selectpicker();
            }
        })
        .catch((error) => {});
});

$(".pg_attr_list").on("click", ".cancel_update_attribute_to_pg", function () {
    id = $(this).data("id");
    btn_id = $(this).attr("id");

    attr_value = $("#attr_field_" + id).val();
    options = $("#attr_options_" + id).val();

    data = {
        id: id,
        title: $("#attr_title_" + id).val(),
        attr: $("#id_selected_" + attr_value).data("value"),
        options: options,
    };

    attr_fields_list = "";

    attr_updated_field =
        `
                        <div class="kt-widget2__checkbox">

                        </div>
                        <div class="kt-widget2__info">
                            <a href="#" onclick="return false;" class="kt-widget2__title">
                                ` +
        data.title +
        `
                            </a>
                            <a href="#" onclick="return false;" class="kt-widget2__username">
                                ` +
        data.attr +
        `
                            </a>
                            <a href="#" onclick="return false;" class="kt-widget2__username">
                                ` +
        data.options +
        `
                            </a>
                        </div>
                        <div class="kt-widget2__actions">
                            <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                <i class="flaticon-more-1"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a data-id="` +
        data.id +
        `"  class="kt-nav__link edit-item">
                                            <i class="kt-nav__link-icon flaticon2-gear"></i>
                                            <span class="kt-nav__link-text">Edit</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a data-id="` +
        data.id +
        `" class="kt-nav__link remove-item">
                                            <i class="kt-nav__link-icon flaticon2-trash"></i>
                                            <span class="kt-nav__link-text">Remove</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    `;
    $(".attr-item-" + id).addClass("kt-widget2__item kt-widget2__item--brand");
    $(".attr-item-" + id).html(attr_updated_field);
    $(".load-participant-group-attribute-fields").selectpicker();
});

$("#update-item-btn").click(function () {
    $("#update-item-btn")
        .addClass(
            "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
        )
        .html("loading...");

    btn_id = $(this).attr("id");
    url = $(this).data("url");
    redirect_url = $(".back-btn").attr("href");
    data = {
        pg_title: $("#edit_pg_title").val(),
    };

    updateItem(btn_id, url, data)
        .then((data) => {
            if (data.type == "Success") {
                $("#update-item-btn")
                    .addClass(
                        "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
                    )
                    .html("loading...");

                if (redirect_url != null) {
                    setTimeout(() => {
                        window.location.href = redirect_url;
                    }, 2000);
                }
            } else {
            }
        })
        .catch((error) => {});
});
