// begin first table
pulseDtTable = $("#kt_table_1").DataTable({
    responsive: true,
    searchDelay: 500,
    processing: true,
    serverSide: true,
    ajax: "participant-groups",
    language: {
        processing: `
                <div class="indicator"> 
                    <svg width="16px" height="12px">
                        <polyline id="back" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
                        <polyline id="front" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
                    </svg>
                </div>
                `,
    },
    columns: [
        { data: "title", name: "title" },
        { data: "status", name: "status" },
        { data: "actions", responsivePriority: -1 },
    ],
    columnDefs: [
        {
            targets: -1,
            width: "100px",
            title: "Actions",
            orderable: false,
            render: function (data, type, full, meta) {
                return (
                    `
                        <a href="participant-groups/` +
                    full.id +
                    `/edit" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Edit Participant Group">							
                            <i class="flaticon2-gear"></i>						
                        </a>
                        <a  onclick="removeDtItem('` +
                    full.id +
                    `','participant-groups/` +
                    full.id +
                    `')" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Remove Participant Group">							
                            <i class="flaticon2-rubbish-bin"></i>						
                        </a>
                        `
                );
            },
        },
        {
            targets: -2,
            render: function (data, type, full, meta) {
                var status = {
                    0: {
                        title: "Disabled",
                        class: "kt-badge--danger",
                        action: "Enable",
                        change: "1",
                    },
                    1: {
                        title: "Enabled",
                        class: " kt-badge--success",
                        action: "Disable",
                        change: "0",
                    },
                };
                if (typeof status[data] === "undefined") {
                    return data;
                }
                return (
                    `
                            <div class="kt-badge ` +
                    status[data].class +
                    ` kt-badge--inline kt-badge--pill">
                                <a class="dropdown-toggle" style="color: white" data-toggle="dropdown" href="#">` +
                    status[data].title +
                    `</a>
                                <div class="dropdown-menu">
                                    <button class="dropdown-item" onclick="change_status('` +
                    status[data].change +
                    `', '` +
                    full.id +
                    `', 'participant-groups/change-status')">` +
                    status[data].action +
                    `</button>
                                </div>
                            </div>
                        `
                );
            },
        },
    ],
});

$(".create-pg-btn").click(function () {
    var check_if_exists = $(this).data("check");
    var check_url = $(this).data("checkurl");
    var btn = $(this);

    btn.addClass(
        "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
    ).html("loading...");

    $.ajax({
        type: "GET",
        url: check_url,
        dataType: "json",
        success: function (response) {
            if (response.exists == "false") {
                notify_toast(
                    "la la-flask",
                    "",
                    "info-type",
                    "Hang on while we set up things for you...",
                    "bottom",
                    "center"
                );

                setTimeout(() => {
                    load_page = btn.data("url") + "?type=" + btn.data("type");
                    window.location.href = load_page;
                }, 2000);
            } else if (response.exists == "true") {
                btn.removeClass(
                    "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
                ).html('<i class="la la-plus"></i>New Participant Group');

                $("#unsaved_participant_group").modal({
                    backdrop: "static",
                    keyboard: false,
                });
            }
        },
        error: function (response) {
            btn.removeClass(
                "kt-spinner kt-spinner--left kt-spinner--sm kt-spinner--light"
            ).html('<i class="la la-plus"></i>New Participant Group');

            if (response.status == "401") {
                notify_toast(
                    "la la-warning",
                    "Warning",
                    "warning-type",
                    "session expired. please login to continue"
                );

                setTimeout(() => {
                    location.reload();
                }, 1000);
            } else {
                notify_toast(
                    "la la-warning",
                    "Warning",
                    "warning-type",
                    "unknown error occurred. please contact management"
                );
            }
        },
    });
});
