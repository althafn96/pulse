jQuery(document).ready(function () {
    $(".load-survey-category-attribute-fields").selectpicker();

    if ($("#attr_list").attr("data-attrListCount") == "0") {
        $("#attr_list").css("display", "none");
    }
});

$(".kt_repeater_1").repeater({
    initEmpty: false,

    defaultValues: {
        "text-input": "foo",
    },

    show: function () {
        $(this).slideDown();
    },

    hide: function (deleteElement) {
        $(this).slideUp(deleteElement);
    },
});

$("#attr_field").change(function () {
    attr_code = $("#attr_field").val();

    if (attr_code == "002" || attr_code == "003" || attr_code == "004") {
        $(".options_section").removeAttr("style");
    } else {
        $('input[name*="attr_options"]').each(function (e) {
            $(this).val("");
        });

        $(".options_section").css("display", "none");
    }
});

Sortable.create(sc_attr_list, {
    scroll: true,
    store: {
        set: function (sortable) {
            // let order = sortable.toArray();
            // console.log(order);

            let order = {};
            $(".kt-widget2__item").each(function () {
                order[$(this).data("id")] = $(this).index();
            });
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                },
            });
            $.ajax({
                url: "change-temp-attr-order",
                type: "POST",
                data: order,
                success: function (response) {
                    console.log(response);
                },
                error: function (response) {
                    if (response.status == "401") {
                        notify_toast(
                            "la la-warning",
                            "Warning",
                            "warning-type",
                            "session expired. please login to continue"
                        );

                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    }
                },
            });
        },
    },
});
