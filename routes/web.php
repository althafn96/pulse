<?php

Route::get('/', function () {
    return view('index');
});

Route::get('/login', 'LoginController@show')->name('login')->middleware('guest');
Route::get('/register', 'RegistrationController@show')
    ->name('register')
    ->middleware('guest');


Route::post('/login', 'LoginController@authenticate')->name('login');
Route::post('/register', 'RegistrationController@register');


Route::middleware('auth')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::redirect('/home', url('/'));
    Route::get('/surveys', 'SurveyController@index')->name('surveys.index');
    Route::get('/surveys/get-surveys', 'SurveyController@getSurveys');
    Route::post('/surveys/create-survey', 'SurveyController@createSurvey');

    Route::get('/surveys/create/{id}', 'SurveyController@designSurvey');

    Route::get('/survey-categories/load-survey-categories-to-select', 'SurveyCategoryController@loadSurveyCategoriesToSelect');

    Route::post('/surveys/update/{survey_id}', 'SurveyController@update');

    Route::get('/surveys/fetch-answers', 'AnswerController@fetchAnswers');

    Route::get('/surveys/fetch-questions', 'QuestionController@fetchQuestions');
    Route::post('/surveys/add-question/{survey_id}', 'QuestionController@addQuestion');
    Route::post('/surveys/change-question-order', 'QuestionController@changeQuestionOrder');
    Route::get('/surveys/change-answers-order/{question_id}', 'AnswerController@getQuestionAnswers');
    Route::post('/surveys/change-answers-order', 'AnswerController@changeAnswerOrder');
    Route::get('/surveys/edit-survey-question', 'QuestionController@editSurveyQuestion');
    Route::post('/surveys/update-question', 'QuestionController@updateQuestion');
    Route::delete('surveys/remove-survey-question', 'QuestionController@removeQuestion');
    Route::get('surveys/get-question', 'QuestionController@getQuestion');

    Route::get('survey-categories/get-survey-categories', 'SurveyCategoryController@getSurveyCategories');
    Route::get('surveys-categories/load-attributes-to-select', 'SurveyCategoryController@load_attributes_to_select');
    Route::get('surveys-categories/get-attributes', 'SurveyCategoryController@getTempAttributes');
    Route::post('survey-categories/change-temp-attr-order', 'SurveyCategoryController@changeTempAttrOrder');


    Route::get('survey-categories/edit-sc-title/{id}', 'SurveyCategoryController@editScTitle');


    Route::post('survey-categories/change-status', 'SurveyCategoryController@changeStatus');
    Route::post('participant-groups/change-status', 'ParticipantGroupController@changeStatus');
    Route::post('participants/change-status', 'ParticipantController@changeStatus');

    Route::post('survey-categories/add-temp-attribute', 'SurveyCategoryController@addTempAttr');
    Route::post('participant-groups/add-temp-attribute', 'ParticipantGroupController@addTempAttr');

    Route::get('survey-categories/add-temp-title', 'SurveyCategoryController@addTempScTitle');
    Route::get('participant-groups/add-temp-title', 'ParticipantGroupController@addTempPgTitle');

    Route::get('survey-categories/edit-temp-attr', 'SurveyCategoryController@editTempAttr');
    Route::get('participant-groups/edit-temp-attr', 'ParticipantGroupController@editTempAttr');

    Route::post('survey-categories/update-temp-attr', 'SurveyCategoryController@updateTempAttr');
    Route::post('participant-groups/update-temp-attr', 'ParticipantGroupController@updateTempAttr');

    Route::delete('survey-categories/remove-temp-attr', 'SurveyCategoryController@removeTempAttr');
    Route::delete('participant-groups/remove-temp-attr', 'ParticipantGroupController@removeTempAttr');

    Route::get('survey-categories/check-unsaved-exists', 'SurveyCategoryController@checkIfUnsavedExists');
    Route::get('participant-groups/check-unsaved-exists', 'ParticipantGroupController@checkIfUnsavedExists');

    Route::post('survey-categories/add-attribute', 'SurveyCategoryAttributes@addAttr');
    Route::post('participant-groups/add-attribute', 'ParticipantGroupAttributeController@addAttr');

    Route::get('survey-categories/edit-attr', 'SurveyCategoryAttributes@editAttr');
    Route::get('participant-groups/edit-attr', 'ParticipantGroupAttributeController@editAttr');

    Route::post('survey-categories/update-attr', 'SurveyCategoryAttributes@updateAttr');
    Route::post('participant-groups/update-attr', 'ParticipantGroupAttributeController@updateAttr');

    Route::delete('survey-categories/remove-attr', 'SurveyCategoryAttributes@removeAttr');
    Route::delete('participant-groups/remove-attr', 'ParticipantGroupAttributeController@removeAttr');

    Route::post('survey-categories/{id}/change-attr-order', 'SurveyCategoryAttributes@changeAttrOrder');
    Route::post('participant-groups/{id}/change-attr-order', 'ParticipantGroupAttributeController@changeAttrOrder');

    Route::post('participant-groups/update/{id}', 'ParticipantGroupController@update');
    // Route::get('survey-categories/create', 'SurveyCategoryController');
    Route::get('participants/get-participant-group-attributes', 'ParticipantGroupController@getGroupAttributes');

    // Route::get('/surveys/load-survey-types-to-select', 'SurveyTypeController@loadSurveyTypesToSelect');

    Route::resource('survey-categories', 'SurveyCategoryController');
    Route::resource('participant-groups', 'ParticipantGroupController');
    Route::resource('participants', 'ParticipantController');

    Route::post('/logout', 'LoginController@logout')->name('logout');
});
